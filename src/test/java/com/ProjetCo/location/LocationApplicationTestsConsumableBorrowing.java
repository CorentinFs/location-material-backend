package com.ProjetCo.location;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import com.ProjetCo.location.controller.ConsumableBorrowingController;
import com.ProjetCo.location.dto.ConsumableBorrowingDTO;
import com.ProjetCo.location.model.ConsumableBorrowing;
import com.ProjetCo.location.service.ConsumableBorrowingServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(ConsumableBorrowingController.class)
@Transactional
public class LocationApplicationTestsConsumableBorrowing {
	@Autowired
	private MockMvc mvc;
	
	@Autowired
	private ConsumableBorrowingServiceImpl cbService;
	
	//Global test variables
	private String name = "category1";
	private long userId = 12345678;
	private LocalDate startDate = LocalDate.of(2020, 4, 1);
	private LocalDate endDate = LocalDate.of(2020, 5, 1);
	private LocalDate renderDate = null;
	
	@Test
	public void getListConsumableBorrowing() throws Exception {
		//Consumables Borrowings of the data populate
		ConsumableBorrowing cb1 = cbService.getConsumableBorrowing(1);
		ConsumableBorrowing cb2 = cbService.getConsumableBorrowing(2);

		ConsumableBorrowingDTO cbDTO1 = consumableBorrowingToConsumableBorrowingDTO(cb1);
		ConsumableBorrowingDTO cbDTO2 = consumableBorrowingToConsumableBorrowingDTO(cb2);
				
		//List of the ConsumablesBorrowings DTOs
		ArrayList<ConsumableBorrowingDTO> listCBDTO = new ArrayList<ConsumableBorrowingDTO>();
		listCBDTO.add(cbDTO1);
		listCBDTO.add(cbDTO2);
		
		//Get the list of ConsumableBorrowings
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .get("/rest/consumableborrowing/api/getAllConsumableBorrowings"))
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        String excpetedResponseBody = asJsonString((List<ConsumableBorrowingDTO>) listCBDTO);

        //Test
        assertEquals(excpetedResponseBody, actualResponseBody);
	}
	@Test
	public void getAllConsumableBorrowingRequest() throws Exception {
		//Products Borrowings of the data populate
		ConsumableBorrowing cb1 = cbService.getConsumableBorrowing(1);
		ConsumableBorrowing cb2 = cbService.getConsumableBorrowing(2);

		ConsumableBorrowingDTO cbDTO1 = consumableBorrowingToConsumableBorrowingDTO(cb1);
		ConsumableBorrowingDTO cbDTO2 = consumableBorrowingToConsumableBorrowingDTO(cb2);

		//List of the ProductsBorrowings DTOs
		ArrayList<ConsumableBorrowingDTO> listPBDTO = new ArrayList<ConsumableBorrowingDTO>();
		listPBDTO.add(cbDTO1);
		listPBDTO.add(cbDTO2);
		//Get the list of ProductBorrowings
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
				.get("/rest/consumableborrowing/api/getConsumableBorrowingByUserId")
				.param("userId", "123456789"))
				.andReturn();

		String actualResponseBody = mvcResult.getResponse().getContentAsString();
		String excpetedResponseBody = asJsonString((List<ConsumableBorrowingDTO>) listPBDTO);

		//Test
		assertEquals(excpetedResponseBody, actualResponseBody);
	}

	@Test
	public void getAllReturnedConsumableOfUser() throws Exception {
		//Products Borrowings of the data populate
		ConsumableBorrowing cb1 = cbService.getConsumableBorrowing(1);
		ConsumableBorrowing cb2 = cbService.getConsumableBorrowing(2);

		ConsumableBorrowingDTO cbDTO1 = consumableBorrowingToConsumableBorrowingDTO(cb1);
		ConsumableBorrowingDTO cbDTO2 = consumableBorrowingToConsumableBorrowingDTO(cb2);

		//List of the ProductsBorrowings DTOs
		ArrayList<ConsumableBorrowingDTO> listPBDTO = new ArrayList<ConsumableBorrowingDTO>();
		listPBDTO.add(cbDTO1);
		listPBDTO.add(cbDTO2);
		//Get the list of ProductBorrowings
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
				.get("/rest/consumableborrowing/api/getReturnedConsumableBorrowingByUserId")
				.param("userId", "123456789"))
				.andReturn();

		String actualResponseBody = mvcResult.getResponse().getContentAsString();
		String excpetedResponseBody = asJsonString((List<ConsumableBorrowingDTO>) listPBDTO);

		//Test
		assertEquals(excpetedResponseBody, actualResponseBody);
	}
	@Test
    public void createAndDelete() throws Exception{
		ConsumableBorrowingDTO newCB = new ConsumableBorrowingDTO(name, userId, startDate, endDate, renderDate);
		String jSonCB = asJsonString(newCB);

        //Create a new ConsumableBorrowing
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                	.post("/rest/consumableborrowing/api/createConsumableBorrowing")
                	.contentType(MediaType.APPLICATION_JSON)
                	.content(jSonCB)
                	.accept(MediaType.APPLICATION_JSON))
					.andExpect(MockMvcResultMatchers.status().isCreated())
                	.andReturn();
        
        //JSon to DTO
        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        ConsumableBorrowingDTO responseDTO = jSonAsConsumableBorrowing(actualResponseBody);

        //Delete the new ConsumableBorrowing
        ConsumableBorrowing cb = cbService.getConsumableBorrowing(responseDTO.getId());
        mvc.perform(MockMvcRequestBuilders
                .delete("/rest/consumableborrowing/api/deleteConsumableBorrowing/" + Integer.toString(cb.getId())))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }
	
	@Test
	public void updateExistingConsumableBorrowing() throws Exception
	{
		/* Get the ConsumableBorrowing to modify */
		ConsumableBorrowing cb = cbService.getConsumableBorrowing(2);
		
		/* Apply the modification */
		LocalDate newDate = LocalDate.now();
		cb.setRenderDate(newDate);
		
		/* Create a DTO of this ConsumableBorrowing to send to the DataBase */
		ConsumableBorrowingDTO consumableBorrowingDTO = consumableBorrowingToConsumableBorrowingDTO(cb);
		String jsonDTO = asJsonString(consumableBorrowingDTO);
		
		/* Process the request */
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
				.put("/rest/consumableborrowing/api/updateConsumableBorrowing")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonDTO)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isCreated())
				.andReturn();

		assertEquals(jsonDTO, mvcResult.getResponse().getContentAsString());
	}
	
	/**
     * Method to transform an object into a Json object. Used to give json as Request Body in requests.
     * @param obj : the object to transform in Json String.
     * @return
     */
    public static String asJsonString(Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
	
    /**
     * Method to transform a Json object into a ConsumableBorrowingDTO
     * @param json : the json string to transorm into a ConsumableBorrowingDTO
     * @return
     */
	public static ConsumableBorrowingDTO jSonAsConsumableBorrowing(String json) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			//JSON string to Java Object
			ConsumableBorrowingDTO cbDTO = mapper.readValue(json, ConsumableBorrowingDTO.class);
			return cbDTO;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Convert a ConsumableBorrowing into a ConsumableBorrowingDTO
	 * @param consumableBorrowing : object to convert
	 * @return
	 */
	public ConsumableBorrowingDTO consumableBorrowingToConsumableBorrowingDTO(ConsumableBorrowing consumableBorrowing) {
		ConsumableBorrowingDTO consumableBorrowingDTO = new ConsumableBorrowingDTO(consumableBorrowing.getId(), 
				consumableBorrowing.getName(), 
				consumableBorrowing.getUserId(), 
				consumableBorrowing.getStartDate(), 
				consumableBorrowing.getEndDate(), 
				consumableBorrowing.getRenderDate());
		return consumableBorrowingDTO;
	}
}

package com.ProjetCo.location;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import com.ProjetCo.location.controller.ArticleController;

@WebMvcTest(ArticleController.class)
@Transactional
public class LocationApplicationTestsConnexion {
	@Autowired
	private MockMvc mvc;
	
	@Test
	public void conectValidUser() throws Exception {
		//Use a User or an Admin created in the populate of the database
		long userId = 356012453;
		String password = "0b14d501a594442a01c6859541bcb3e8164d183d32937b851835442f69d5c94e";
		
		mvc.perform(MockMvcRequestBuilders
                .get("/rest/connexion/api/connect")
                .param("userId", Long.toString(userId))
                .param("password", password))
                .andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void conectNotValidUser() throws Exception {
		//Use a User or an Admin created in the populate of the database
		long userId = 356012453;
		String password = "notexistingpassword";
		
		mvc.perform(MockMvcRequestBuilders
                .get("/rest/connexion/api/connect")
                .param("userId", Long.toString(userId))
                .param("password", password))
                .andExpect(MockMvcResultMatchers.status().isConflict());
	}
}

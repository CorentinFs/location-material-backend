package com.ProjetCo.location;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import com.ProjetCo.location.controller.ConsumableRequestController;
import com.ProjetCo.location.dto.ConsumableRequestDTO;
import com.ProjetCo.location.model.ConsumableRequest;
import com.ProjetCo.location.service.ConsumableRequestServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(ConsumableRequestController.class)
@Transactional
public class LocationApplicationTestsConsumableRequest {
	@Autowired
	private MockMvc mvc;
	
	@Autowired
	private ConsumableRequestServiceImpl crService;
	
	//Global test variables
	private String name = "category1";
	private long userId = 12345678;
	private LocalDate startDate = LocalDate.of(2020, 4, 1);
	private LocalDate endDate = LocalDate.of(2020, 5, 1);
	
	@Test
	public void getListConsumableRequest() throws Exception {
		//Consumables Requests of the data populate
		ConsumableRequest cr1 = crService.getConsumableRequest(1);
		ConsumableRequest cr2 = crService.getConsumableRequest(2);

		ConsumableRequestDTO crDTO1 = consumableRequestToConsumableRequestDTO(cr1);
		ConsumableRequestDTO crDTO2 = consumableRequestToConsumableRequestDTO(cr2);
				
		//List of the ConsumableRequests DTOs
		ArrayList<ConsumableRequestDTO> listCRDTO = new ArrayList<ConsumableRequestDTO>();
		listCRDTO.add(crDTO1);
		listCRDTO.add(crDTO2);
		
		//Get the list of ConsumableRequests
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .get("/rest/consumablerequest/api/getAllConsumableRequests"))
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        String excpetedResponseBody = asJsonString((List<ConsumableRequestDTO>) listCRDTO);
        //Test
        assertEquals(excpetedResponseBody, actualResponseBody);
	}

	@Test
	public void validateConsumableRequest() throws Exception {
		ConsumableRequest cr1 = crService.getConsumableRequest(1);
		mvc.perform(MockMvcRequestBuilders
				.post("/rest/consumablerequest/api/validateConsumableRequest/" + Integer.toString(cr1.getId())))
				.andExpect(MockMvcResultMatchers.status().isNoContent());

		mvc.perform(MockMvcRequestBuilders
				.get("/rest/consumableborrowing/api/getConsumableBorrowingByUserId")
				.param("userId", "123456789"))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void getConsumableRequestByUserId() throws Exception
	{
		/* Get the existing ConsumableRequest from the populate */
		ConsumableRequest cr1 = crService.getConsumableRequest(1);
		ConsumableRequest cr2 = crService.getConsumableRequest(2);
		
		/* Insert thme into a List */
		List<ConsumableRequest> crList = new ArrayList<ConsumableRequest>();
		crList.add(cr1);
		crList.add(cr2);
		
		/* Process the request */
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
				.get("/rest/consumablerequest/api/getConsumableRequestByUserId")
				.param("userId", "123456789"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andReturn();
		
		assertEquals(asJsonString((List<ConsumableRequest>) crList), mvcResult.getResponse().getContentAsString());
		
	}
	
	
	@Test
    public void createAndDelete() throws Exception{
		ConsumableRequestDTO newCR = new ConsumableRequestDTO(name, userId, startDate, endDate);
		String jSonCR = asJsonString(newCR);

        //Create a new ConsumableRequest
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                	.post("/rest/consumablerequest/api/createConsumableRequest")
                	.contentType(MediaType.APPLICATION_JSON)
                	.content(jSonCR)
                	.accept(MediaType.APPLICATION_JSON))
					.andExpect(MockMvcResultMatchers.status().isCreated())
                	.andReturn();
        
        //JSon to DTO
        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        ConsumableRequestDTO responseDTO = jSonAsConsumableRequest(actualResponseBody);

        //Delete the new ConsumableAdministrator
        ConsumableRequest cr = crService.getConsumableRequest(responseDTO.getId());
        mvc.perform(MockMvcRequestBuilders
                .delete("/rest/consumablerequest/api/deleteConsumableRequest/" + Integer.toString(cr.getId())))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }
	
	/**
     * Method to transform an object into a Json object. Used to give json as Request Body in requests.
     * @param obj : the object to transform in Json String.
     * @return
     */
    public static String asJsonString(Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
	
    /**
     * Method to transform a Json object into a ConsumableRequestDTO
     * @param json : the json string to transorm into a ConsumableRequestDTO
     * @return
     */
	public static ConsumableRequestDTO jSonAsConsumableRequest(String json) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			//JSON string to Java Object
			ConsumableRequestDTO cbDTO = mapper.readValue(json, ConsumableRequestDTO.class);
			return cbDTO;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Transform a ConsumableRequest into a ConsumableRequestDTO entity object
	 * @param ConsumableRequest : the ConsumableRequest to transform
	 * @return ConsumableRequestDTO : the ConsumableRequestDTO entity corresponding
	 */
	public ConsumableRequestDTO consumableRequestToConsumableRequestDTO(ConsumableRequest consumableRequest) {
		return new ConsumableRequestDTO(consumableRequest.getId(), consumableRequest.getName(), consumableRequest.getUserId(), consumableRequest.getStartDate(), 
				consumableRequest.getEndDate());
	}

}

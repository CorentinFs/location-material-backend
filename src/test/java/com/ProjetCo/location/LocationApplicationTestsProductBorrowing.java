package com.ProjetCo.location;

import com.ProjetCo.location.controller.ProductBorrowingController;
import com.ProjetCo.location.dto.ProductBorrowingDTO;
import com.ProjetCo.location.model.Article;
import com.ProjetCo.location.model.Category;
import com.ProjetCo.location.model.Product;
import com.ProjetCo.location.model.ProductBorrowing;
import com.ProjetCo.location.service.ArticleServiceImpl;
import com.ProjetCo.location.service.CategoryServiceImpl;
import com.ProjetCo.location.service.ProductBorrowingServiceImpl;
import com.ProjetCo.location.service.ProductServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
@Transactional
@WebMvcTest(ProductBorrowingController.class)
public class LocationApplicationTestsProductBorrowing {
	@Autowired
	private MockMvc mvc;
	
	@Autowired
	private ArticleServiceImpl articleService;
	
	@Autowired
	private CategoryServiceImpl categoryService;

	@Autowired
	private ProductServiceImpl productService;

	@Autowired
	private ProductBorrowingServiceImpl productBorrowingService;

	//Global test variables
	private String name = "name";
	private String description = "desc";
	private double barcode = 123456;
	private String state = "state";
	private boolean available = true;
	private boolean reserved = false;
	private long userId = 12345678;
	private LocalDate startDate = LocalDate.of(2020, 4, 1);
	private LocalDate endDate = LocalDate.of(2020, 5, 1);
	private LocalDate renderDate = null;
	private String adminComment;
	private boolean pickedUp;

	@Test
	public void getListProductBorrowing() throws Exception {
		//Products Borrowings of the data populate
		ProductBorrowing pb1 = productBorrowingService.getProductBorrowing(1);
		ProductBorrowing pb2 = productBorrowingService.getProductBorrowing(2);
		ProductBorrowing pb3 = productBorrowingService.getProductBorrowing(3);

		ProductBorrowingDTO pbDTO1 = productBorrowingToProductBorrowingDTO(pb1);
		ProductBorrowingDTO pbDTO2 = productBorrowingToProductBorrowingDTO(pb2);
		ProductBorrowingDTO pbDTO3 = productBorrowingToProductBorrowingDTO(pb3);
				
		//List of the ProductsBorrowings DTOs
		ArrayList<ProductBorrowingDTO> listPBDTO = new ArrayList<ProductBorrowingDTO>();
		listPBDTO.add(pbDTO1);
		listPBDTO.add(pbDTO2);
		listPBDTO.add(pbDTO3);
		
		//Get the list of ProductBorrowings
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .get("/rest/productborrowing/api/getAllProductBorrowings"))
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        String excpetedResponseBody = asJsonString((List<ProductBorrowingDTO>) listPBDTO);
        
        //Test
        assertEquals(excpetedResponseBody, actualResponseBody);
	}
	@Test
	public void getProductBorrowingParam() throws Exception {
		//Products Borrowings of the data populate
		ProductBorrowing pb = productBorrowingService.getProductBorrowing(1);

		ProductBorrowingDTO pbDTO = productBorrowingToProductBorrowingDTO(pb);

		//Get the list of ProductBorrowings
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
				.get("/rest/productborrowing/api/getProductBorrowing")
				.param("userId", "123456789")
				.param("startDate", pb.getStartDate().toString())
				.param("endDate", pb.getEndDate().toString())
				.param("productId", Integer.toString(pb.getProduct().getId())))
				.andReturn();

		String actualResponseBody = mvcResult.getResponse().getContentAsString();
		String excpetedResponseBody = asJsonString(pbDTO);

		//Test
		assertEquals(excpetedResponseBody, actualResponseBody);
	}
	@Test
	public void getAllProductBorrowingRequest() throws Exception {
		//Products Borrowings of the data populate
		ProductBorrowing pb1 = productBorrowingService.getProductBorrowing(1);
		ProductBorrowing pb2 = productBorrowingService.getProductBorrowing(2);
		ProductBorrowing pb3 = productBorrowingService.getProductBorrowing(3);

		ProductBorrowingDTO pbDTO1 = productBorrowingToProductBorrowingDTO(pb1);
		ProductBorrowingDTO pbDTO2 = productBorrowingToProductBorrowingDTO(pb2);
		ProductBorrowingDTO pbDTO3 = productBorrowingToProductBorrowingDTO(pb3);

		//List of the ProductsBorrowings DTOs
		ArrayList<ProductBorrowingDTO> listPBDTO = new ArrayList<ProductBorrowingDTO>();
		listPBDTO.add(pbDTO1);
		listPBDTO.add(pbDTO2);
		listPBDTO.add(pbDTO3);
		
		//Get the list of ProductBorrowings
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
				.get("/rest/productborrowing/api/getProductBorrowingByUserId")
				.param("userId", "123456789"))
				.andReturn();

		String actualResponseBody = mvcResult.getResponse().getContentAsString();
		String excpetedResponseBody = asJsonString((List<ProductBorrowingDTO>) listPBDTO);

		//Test
		assertEquals(excpetedResponseBody, actualResponseBody);
	}
	@Test
	public void getAllReturnedProductOfUser() throws Exception {
		//Products Borrowings of the data populate
		ProductBorrowing pb2 = productBorrowingService.getProductBorrowing(2);

		ProductBorrowingDTO pbDTO2 = productBorrowingToProductBorrowingDTO(pb2);

		//List of the ProductsBorrowings DTOs
		ArrayList<ProductBorrowingDTO> listPBDTO = new ArrayList<ProductBorrowingDTO>();
		listPBDTO.add(pbDTO2);
		
		//Get the list of ProductBorrowings
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
				.get("/rest/productborrowing/api/getReturnedProductBorrowingByUserId")
				.param("userId", "123456789"))
				.andReturn();

		String actualResponseBody = mvcResult.getResponse().getContentAsString();
		String excpetedResponseBody = asJsonString((List<ProductBorrowingDTO>) listPBDTO);

		//Test
		assertEquals(excpetedResponseBody, actualResponseBody);
	}
	
	@Test
    public void createAndDelete() throws Exception{
		Category category = new Category("c1", new ArrayList<Article>());
		categoryService.saveCategory(category);
		Article article = new Article(name, description, category, new ArrayList<Product>(), null);
		articleService.saveArticle(article);
		Product product = new Product(barcode,state,available,reserved,article);
		productService.saveProduct(product);

		ProductBorrowingDTO productBorrowingDTO = new ProductBorrowingDTO(userId,startDate,endDate,renderDate, adminComment, pickedUp, product);
		String jSonCB = asJsonString(productBorrowingDTO);

		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                	.post("/rest/productborrowing/api/createProductBorrowing")
                	.contentType(MediaType.APPLICATION_JSON)
                	.content(jSonCB)
                	.accept(MediaType.APPLICATION_JSON))
					.andExpect(MockMvcResultMatchers.status().isCreated())
                	.andReturn();
        
        //JSon to DTO
        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        ProductBorrowingDTO responseDTO = jSonAsProductBorrowing(actualResponseBody);
		ProductBorrowing productBorrowing = productBorrowingService.getProductBorrowing(responseDTO.getId());

        mvc.perform(MockMvcRequestBuilders
                .delete("/rest/productborrowing/api/deleteProductBorrowing/" + Integer.toString(productBorrowing.getId())))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

	}
	
	@Test
	public void updateExistingProductBorrowing() throws Exception
	{
		/* Get the ProductBorrowing to modify */
		ProductBorrowing pb = productBorrowingService.getProductBorrowing(1);
		
		/* Apply the modification */
		pb.setAdminComment("Rendu en bon etat");
		pb.setEndDate(LocalDate.now());
		
		/* Create a DTO of this ProductBorrowing to send to the DataBase */
		ProductBorrowingDTO productBorrowingDTO = fromProductBorrowingToDTO(pb);
		String jsonDTO = asJsonString(productBorrowingDTO);
		
		/* Process the request */
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
				.put("/rest/productborrowing/api/updateProductBorrowing")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonDTO)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isCreated())
				.andReturn();

		assertEquals(jsonDTO, mvcResult.getResponse().getContentAsString());
	}
	
	public ProductBorrowingDTO fromProductBorrowingToDTO(ProductBorrowing productBorrowing) 
	{
		ProductBorrowingDTO res = new ProductBorrowingDTO(productBorrowing.getId(), 
				productBorrowing.getUserId(), 
				productBorrowing.getStartDate(), 
				productBorrowing.getEndDate(), 
				productBorrowing.getRenderDate(), 
				productBorrowing.getAdminComment(), 
				productBorrowing.getPickedUp(), 
				productBorrowing.getProduct());
		
		return res;
	}
	
	/**
     * Method to transform an object into a Json object. Used to give json as Borrowing Body in borrowings.
     * @param obj : the object to transform in Json String.
     * @return
     */
    public static String asJsonString(Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
	
    /**
     * Method to transform a Json object into a ArticleDTO
     * @param json : the json string to transorm into a ArticleDTO
     * @return
     */
	public static ProductBorrowingDTO jSonAsProductBorrowing(String json) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			//JSON string to Java Object
			ProductBorrowingDTO cbDTO = mapper.readValue(json, ProductBorrowingDTO.class);
			return cbDTO;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	 /**
     * Convert a list of ProductBorrowing into a list of ProductBorrowingDTO
     * @param productBorrowingList
     * @return
     */
     public ProductBorrowingDTO productBorrowingToProductBorrowingDTO(ProductBorrowing productBorrowing) {
    	 ProductBorrowingDTO productBorrowingDTO =  new ProductBorrowingDTO(productBorrowing.getId(), productBorrowing.getUserId(), 
        		productBorrowing.getStartDate(), productBorrowing.getEndDate(), productBorrowing.getRenderDate(), 
        		productBorrowing.getAdminComment(), productBorrowing.getPickedUp(), productBorrowing.getProduct());

         return productBorrowingDTO;
     }

}

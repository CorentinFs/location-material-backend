package com.ProjetCo.location;

import com.ProjetCo.location.controller.ArticleController;
import com.ProjetCo.location.dto.ArticleDTO;
import com.ProjetCo.location.model.Article;
import com.ProjetCo.location.model.Category;
import com.ProjetCo.location.model.Product;
import com.ProjetCo.location.service.ArticleServiceImpl;
import com.ProjetCo.location.service.CategoryServiceImpl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.transaction.annotation.Transactional;


import static org.junit.jupiter.api.Assertions.assertEquals;

@WebMvcTest(ArticleController.class)
@Transactional
public class LocationApplicationTestsArticle {
	@Autowired
	private MockMvc mvc;
	
	@Autowired
	private ArticleServiceImpl articleService;
	
	@Autowired
	private CategoryServiceImpl categoryService;
	
	//Global test variables
	private String name = "name";
	private String description = "desc";
	
	@Test
	public void updateExistingArticle() throws Exception
	{
		/* Get the Article to modify of the data populate*/
		Article article = articleService.getArticle(1).get();
		
		/* Apply the modification */
		article.setDescription("En cours de test");
		
		/* Create a DTO of this Article to send to the DataBase */
		ArticleDTO articleDTO = articleToArticleDTO(article);
		String jsonDTO = asJsonString(articleDTO);
		
		/* Process the request */
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
				.put("/rest/article/api/updateArticle")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonDTO)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isCreated())
				.andReturn();
		
		assertEquals(jsonDTO, mvcResult.getResponse().getContentAsString());
	}
	
	@Test
	public void getNotAvailableArticles() throws Exception {
		mvc.perform(MockMvcRequestBuilders
				.get("/rest/article/api/getNotAvailableProducts")
				.param("id", "1"))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void getArticleByName() throws Exception
	{
		/* Get the Article from the populate */
		Article article = articleService.getArticle(1).get();
		ArticleDTO articleDTO = articleToArticleDTO(article);
		
		/* Process the request */
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
				.get("/rest/article/api/getArticleByName")
				.param("name", "Article 1"))
				.andExpect(MockMvcResultMatchers.status().isOk())
            	.andReturn();
		
		assertEquals(asJsonString(articleDTO), mvcResult.getResponse().getContentAsString());
	}
	
	@Test
	public void getArticleById() throws Exception {
		//Create a temporary Category to create the article
		Category category = new Category("c1", new ArrayList<Article>());
		
		categoryService.saveCategory(category);
		
		ArticleDTO articleDTO = new ArticleDTO(name, description, category, new ArrayList<Product>(), null);
		String jSonCB = asJsonString(articleDTO);
		
		//Create a new Article
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
					.post("/rest/article/api/createArticle")
					.contentType(MediaType.APPLICATION_JSON)
					.content(jSonCB)
					.accept(MediaType.APPLICATION_JSON))
					.andExpect(MockMvcResultMatchers.status().isCreated())
                	.andReturn();
		
		ArticleDTO articleCreated = jSonAsArticle(mvcResult.getResponse().getContentAsString());
		
		MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders
                .get("/rest/article/api/getArticleById")
                .param("id", Integer.toString(articleCreated.getId())))
                .andReturn();
		
		assertEquals(mvcResult.getResponse().getContentAsString(), mvcResult2.getResponse().getContentAsString());
	}
	
	@Test
	public void getListArticle() throws Exception {
		//Articles of the data populate
		Optional<Article> optArticle1 = articleService.getArticle(1);
		Optional<Article> optArticle2 = articleService.getArticle(2);
		
		Article article1 = optArticle1.get();
		Article article2 = optArticle2.get();

		ArticleDTO articleDTO1 = articleToArticleDTO(article1);
		ArticleDTO articleDTO2 = articleToArticleDTO(article2);
		
		//List of the articles DTOs
		ArrayList<ArticleDTO> listArticleDTO = new ArrayList<ArticleDTO>();
		listArticleDTO.add(articleDTO1);
		listArticleDTO.add(articleDTO2);
		
		//Get the list of Article
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .get("/rest/article/api/getAllArticles"))
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        String excpetedResponseBody = asJsonString((List<ArticleDTO>) listArticleDTO);

        //Test
        assertEquals(excpetedResponseBody, actualResponseBody);
	}
	
	@Test
    public void createAndDelete() throws Exception{
		//Create a temporary Category to create the article
		Category category = new Category("c1", new ArrayList<Article>());
		
		categoryService.saveCategory(category);
		
		ArticleDTO articleDTO = new ArticleDTO(name, description, category, new ArrayList<Product>(), null);
		String jSonCB = asJsonString(articleDTO);

        //Create a new Article
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                	.post("/rest/article/api/createArticle")
                	.contentType(MediaType.APPLICATION_JSON)
                	.content(jSonCB)
                	.accept(MediaType.APPLICATION_JSON))
					.andExpect(MockMvcResultMatchers.status().isCreated())
                	.andReturn();
        
        //JSon to DTO
        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        ArticleDTO responseDTO = jSonAsArticle(actualResponseBody);

        //Delete the new ConsumableAdministrator
        Article cb = articleService.getArticle(responseDTO.getId()).get();
        mvc.perform(MockMvcRequestBuilders
                .delete("/rest/article/api/deleteArticle/" + Integer.toString(cb.getId())))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }
	
	/**
     * Method to transform an object into a Json object. Used to give json as Request Body in requests.
     * @param obj : the object to transform in Json String.
     * @return
     */
    public static String asJsonString(Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
	
    /**
     * Method to transform a Json object into a ArticleDTO
     * @param json : the json string to transorm into a ArticleDTO
     * @return
     */
	public static ArticleDTO jSonAsArticle(String json) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			//JSON string to Java Object
			ArticleDTO cbDTO = mapper.readValue(json, ArticleDTO.class);
			return cbDTO;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
     * Transform a Article into a ArticleDTO entity.
     * @param article : the Article to transform into a DTO.
     * @return ArticleDTO : the ArticleDTO entity corresponding.
     */
    public ArticleDTO articleToArticleDTO(Article article) {
    	return new ArticleDTO(article.getId(), article.getName(), article.getDescription(),
    			article.getCategory(),article.getProducts(), article.getImage());
    }
}

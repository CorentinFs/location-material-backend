package com.ProjetCo.location;

import com.ProjetCo.location.controller.ProductController;
import com.ProjetCo.location.dto.ProductDTO;
import com.ProjetCo.location.model.Article;
import com.ProjetCo.location.model.Category;
import com.ProjetCo.location.model.Product;
import com.ProjetCo.location.service.ArticleServiceImpl;
import com.ProjetCo.location.service.CategoryServiceImpl;
import com.ProjetCo.location.service.ProductServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

@WebMvcTest(ProductController.class)
@Transactional
public class LocationApplicationTestsProduct {
	@Autowired
	private MockMvc mvc;
	
	@Autowired
	private ArticleServiceImpl articleService;
	
	@Autowired
	private CategoryServiceImpl categoryService;

	@Autowired
	private ProductServiceImpl productService;
	//Global test variables
	private String name = "name";
	private String description = "desc";
	private double barcode = 123456;
	private String state = "state";
	private boolean available = true;
	private boolean reserved = false;
	
	@Test
	public void getListProduct() throws Exception {
		//Consumables Requests of the data populate
		double barcode1 = 6512216498465d;
		double barcode2 = 5465465498798d;
		Product p1 = productService.getProduct(barcode1);
		Product p2 = productService.getProduct(barcode2);

		ProductDTO pDTO1 = productToProductDTO(p1);
		ProductDTO pDTO2 = productToProductDTO(p2);
				
		//List of the Products DTOs
		ArrayList<ProductDTO> listPDTO = new ArrayList<ProductDTO>();
		listPDTO.add(pDTO1);
		listPDTO.add(pDTO2);
		
		//Get the list of Products
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .get("/rest/product/api/getAllProducts"))
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        String excpetedResponseBody = asJsonString((List<ProductDTO>) listPDTO);

        //Test
        assertEquals(excpetedResponseBody, actualResponseBody);
	}

	
	@Test
	public void updateExistingProduct() throws Exception
	{
		/* Get the Product to modify */
		Product product = productService.getProduct(6512216498465.d);
		
		/* Apply the modification */
		product.setState("En cours de test");
		
		/* Create a DTO of this Product to send to the DataBase */
		ProductDTO productDTO = productToProductDTO(product);
		String jsonDTO = asJsonString(productDTO);
		
		/* Process the request */
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
				.put("/rest/product/api/updateProduct")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonDTO)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isCreated())
				.andReturn();
		
		assertEquals(jsonDTO, mvcResult.getResponse().getContentAsString());
	}
	
	
	@Test
    public void createAndDelete() throws Exception{
		Category category = new Category("c1", new ArrayList<Article>());
		categoryService.saveCategory(category);
		Article article = new Article(name, description, category, new ArrayList<Product>(), null);
		articleService.saveArticle(article);

		ProductDTO productDTO = new ProductDTO(barcode,state,available,reserved, article, false, false, false);
		String jSonCB = asJsonString(productDTO);

		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                	.post("/rest/product/api/createProduct")
                	.contentType(MediaType.APPLICATION_JSON)
                	.content(jSonCB)
                	.accept(MediaType.APPLICATION_JSON))
					.andExpect(MockMvcResultMatchers.status().isCreated())
                	.andReturn();
        
        //JSon to DTO
        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        ProductDTO responseDTO = jSonAsProduct(actualResponseBody);
		Product product = productService.getProduct((long) responseDTO.getBarcode());

        mvc.perform(MockMvcRequestBuilders
                .delete("/rest/product/api/deleteProduct/" + Double.toString(product.getBarcode())))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
	}
	
	/**
     * Method to transform an object into a Json object. Used to give json as Request Body in requests.
     * @param obj : the object to transform in Json String.
     * @return
     */
    public static String asJsonString(Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
	
    /**
     * Method to transform a Json object into a ArticleDTO
     * @param json : the json string to transorm into a ArticleDTO
     * @return
     */
	public static ProductDTO jSonAsProduct(String json) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			//JSON string to Java Object
			ProductDTO cbDTO = mapper.readValue(json, ProductDTO.class);
			return cbDTO;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
     * Convert a Product into a ProductDTO
     * @param product : object to convert
     * @return
     */
    public ProductDTO productToProductDTO(Product product) {
        ProductDTO productDTO = new ProductDTO(product.getId(), product.getBarcode(), product.getState(), 
        		product.isAvailable(),product.isReserved(),product.getArticle(), product.isTp(), product.isDonation(), product.isHs());
        return productDTO;
    }

}
package com.ProjetCo.location;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import com.ProjetCo.location.controller.CategoryController;
import com.ProjetCo.location.dto.CategoryDTO;
import com.ProjetCo.location.model.Article;
import com.ProjetCo.location.model.Category;
import com.ProjetCo.location.service.CategoryServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.transaction.annotation.Transactional;

@WebMvcTest(CategoryController.class)
@Transactional
class LocationApplicationTestsCategory {

	@Autowired
	private MockMvc mvc;
	
	@Autowired
	private CategoryServiceImpl categoryService;
	
	//Global test variables
	private String categoryName = "c1";
	
	//TODO : test suppression category avec articles liés
	
	@Test
	public void getListOfCategoriesAPI() throws Exception {
		//Categories of the data populate
		Category category1 = categoryService.getCategory("Category 1");
		Category category2 = categoryService.getCategory("Category 2");

		CategoryDTO categoryDTO1 = categoryToCategoryDTO(category1);
		CategoryDTO categoryDTO2 = categoryToCategoryDTO(category2);
				
		//List of the articles DTOs
		ArrayList<CategoryDTO> listCateogryDTO = new ArrayList<CategoryDTO>();
		listCateogryDTO.add(categoryDTO1);
		listCateogryDTO.add(categoryDTO2);
		
		//Get the list of Article
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .get("/rest/category/api/getAllCategories"))
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        String expectedResponseBody = asJsonString((List<CategoryDTO>) listCateogryDTO);
        
        //Test
        assertEquals(expectedResponseBody, actualResponseBody);
	}
	
	@Test
	public void getCategoryByName() throws Exception
	{
		/* Get the Category from the populate */
		Category Category = categoryService.getCategory("Category 1");
		CategoryDTO CategoryDTO = categoryToCategoryDTO(Category);
		
		/* Process the request */
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
				.get("/rest/category/api/getCategoryByName")
				.param("name", "Category 1"))
				.andExpect(MockMvcResultMatchers.status().isOk())
            	.andReturn();
		
		assertEquals(asJsonString(CategoryDTO), mvcResult.getResponse().getContentAsString());
	}
	
	@Test
	public void createCategoryAPI() throws Exception{
		CategoryDTO newCategory = new CategoryDTO(categoryName, new ArrayList<Article>());
		String jSonCategory = asJsonString(newCategory);
		
		mvc.perform(MockMvcRequestBuilders
				.post("/rest/category/api/createCategory")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jSonCategory)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isCreated());
	}
	
	@Test
	public void createCategoryAlreadyExistsAPI() throws Exception{
		CategoryDTO newCategory = new CategoryDTO(categoryName, new ArrayList<Article>());
		String jSonCategory = asJsonString(newCategory);
		
		mvc.perform(MockMvcRequestBuilders
				.post("/rest/category/api/createCategory")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jSonCategory)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isCreated());

		mvc.perform(MockMvcRequestBuilders
				.post("/rest/category/api/createCategory")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jSonCategory)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isConflict());
	}
	
	@Test
	public void getCategoryByIdAPI() throws Exception {
		CategoryDTO newCategory = new CategoryDTO(categoryName, new ArrayList<Article>());
		String jSonCategory = asJsonString(newCategory);
		
		MvcResult mvcResult1 = mvc.perform(MockMvcRequestBuilders
				.post("/rest/category/api/createCategory")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jSonCategory)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isCreated())
				.andReturn();
		
		//JSon to DTO
        String actualResponseBody = mvcResult1.getResponse().getContentAsString();
        
        CategoryDTO responseDTO = jSonAsCategory(actualResponseBody);
		
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
				.get("/rest/category/api/getCategoryById/")
				.param("id", Integer.toString(responseDTO.getId())))
				.andReturn();
		
		//Test
		String actualResponseBody2 = mvcResult.getResponse().getContentAsString();
		assertEquals(actualResponseBody2, actualResponseBody);
	}
	
	@Test
	public void deleteCategoryAPI() throws Exception{
		CategoryDTO newCategory = new CategoryDTO(categoryName, new ArrayList<Article>());
		String jSonCategory = asJsonString(newCategory);

		mvc.perform(MockMvcRequestBuilders
				.post("/rest/category/api/createCategory")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jSonCategory)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isCreated());

		Category category = categoryService.getCategory(categoryName);
		
		mvc.perform(MockMvcRequestBuilders
				.delete("/rest/category/api/deleteCategory/" + Integer.toString(category.getId())))
				.andExpect(MockMvcResultMatchers.status().isNoContent());
	}
	
	@Test
	public void deleteNonExistingCategoryAPI() throws Exception{
		mvc.perform(MockMvcRequestBuilders
				.delete("/rest/category/api/deleteCategory/" + Integer.toString(0)))
				.andExpect(MockMvcResultMatchers.status().isConflict());
	}
	
	/**
	 * Method to transform an object into a Json object. Used to give json as Request Body in requests.
	 * @param obj : the object to transform in Json String.
	 * @return
	 */
	public static String asJsonString(Object obj) {
	    try {
	        final ObjectMapper mapper = new ObjectMapper();
	        final String jsonContent = mapper.writeValueAsString(obj);
	        return jsonContent;
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}
	
	/**
     * Method to transform a Json object into a CategoryDTO
     * @param json : the json string to transorm into a CategoryDTO
     * @return
     */
	public static CategoryDTO jSonAsCategory(String json) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			//JSON string to Java Object
			CategoryDTO catDTO = mapper.readValue(json, CategoryDTO.class);
			return catDTO;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Convert a Category into a CategoryDTO
	 * @param category
	 * @return
	 */
	public CategoryDTO categoryToCategoryDTO(Category category){
		CategoryDTO categoryDTO = new CategoryDTO(category.getId(), category.getName(), category.getArticles());
		
		return categoryDTO;
	}
}

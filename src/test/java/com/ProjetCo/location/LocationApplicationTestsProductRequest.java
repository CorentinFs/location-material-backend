package com.ProjetCo.location;

import com.ProjetCo.location.controller.ProductRequestController;
import com.ProjetCo.location.dto.ProductRequestDTO;
import com.ProjetCo.location.model.Article;
import com.ProjetCo.location.model.Category;
import com.ProjetCo.location.model.Product;
import com.ProjetCo.location.model.ProductRequest;
import com.ProjetCo.location.service.ArticleServiceImpl;
import com.ProjetCo.location.service.CategoryServiceImpl;
import com.ProjetCo.location.service.ProductRequestServiceImpl;
import com.ProjetCo.location.service.ProductServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@WebMvcTest(ProductRequestController.class)
@Transactional
public class LocationApplicationTestsProductRequest {
	@Autowired
	private MockMvc mvc;
	
	@Autowired
	private ArticleServiceImpl articleService;
	
	@Autowired
	private CategoryServiceImpl categoryService;

	@Autowired
	private ProductServiceImpl productService;

	@Autowired
	private ProductRequestServiceImpl productRequestService;

	//Global test variables
	private String name = "name";
	private String description = "desc";
	private double barcode = 123456;
	private String state = "state";
	private boolean available = true;
	private boolean reserved = false;
	private long userId = 12345678;
	private LocalDate startDate = LocalDate.of(2020, 4, 1);
	private LocalDate endDate = LocalDate.of(2020, 5, 1);
	
	@Test
	public void getListProductRequest() throws Exception {
		//Products Requests of the data populate
		ProductRequest pr1 = productRequestService.getProductRequest(1);
		ProductRequest pr2 = productRequestService.getProductRequest(2);

		ProductRequestDTO prDTO1 = productRequestToProductRequestDTO(pr1);
		ProductRequestDTO prDTO2 = productRequestToProductRequestDTO(pr2);
				
		//List of the ProductsRequests DTOs
		ArrayList<ProductRequestDTO> listPBDTO = new ArrayList<ProductRequestDTO>();
		listPBDTO.add(prDTO1);
		listPBDTO.add(prDTO2);
		
		//Get the list of ProductRequests
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .get("/rest/productrequest/api/getAllProductRequests"))
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        String excpetedResponseBody = asJsonString((List<ProductRequestDTO>) listPBDTO);

        //Test
        assertEquals(excpetedResponseBody, actualResponseBody);
	}

	@Test
	public void validateProductRequest() throws Exception {
		ProductRequest pr1 = productRequestService.getProductRequest(1);

		mvc.perform(MockMvcRequestBuilders
				.post("/rest/productrequest/api/validateProductRequest/" + Integer.toString(pr1.getId())))
				.andExpect(MockMvcResultMatchers.status().isNoContent());

		mvc.perform(MockMvcRequestBuilders
				.get("/rest/productborrowing/api/getProductBorrowingByUserId")
				.param("userId", "123456789"))
				.andExpect(MockMvcResultMatchers.status().isOk());
		
		//TODO : finir le test
	}
	@Test
	public void getProductRequestByUser() throws Exception 
	{
		/* Get the existing ProductRequest from the populate */
		ProductRequest pr1 = productRequestService.getProductRequest(1);
		ProductRequest pr2 = productRequestService.getProductRequest(2);
		
		/* Insert them into a List */
		List<ProductRequest> prList = new ArrayList<ProductRequest>();
		prList.add(pr1);
		prList.add(pr2);
		
		/* Process the request */
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
				.get("/rest/productrequest/api/getProductRequestByUserId")
				.param("userId", "123456789"))
				.andExpect(MockMvcResultMatchers.status().isOk())
            	.andReturn();
		
		assertEquals(asJsonString(prList), mvcResult.getResponse().getContentAsString());
	}
	
	
	@Test
    public void createAndDelete() throws Exception{
		Category category = new Category("c1", new ArrayList<Article>());
		categoryService.saveCategory(category);
		Article article = new Article(name, description, category, new ArrayList<Product>(), null);
		articleService.saveArticle(article);
		Product product = new Product(barcode,state,available,reserved,article);
		productService.saveProduct(product);

		ProductRequestDTO productRequestDTO = new ProductRequestDTO(userId,startDate,endDate,product);
		String jSonCB = asJsonString(productRequestDTO);

		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                	.post("/rest/productrequest/api/createProductRequest")
                	.contentType(MediaType.APPLICATION_JSON)
                	.content(jSonCB)
                	.accept(MediaType.APPLICATION_JSON))
					.andExpect(MockMvcResultMatchers.status().isCreated())
                	.andReturn();
        
        //JSon to DTO
        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        ProductRequestDTO responseDTO = jSonAsProductRequest(actualResponseBody);
		ProductRequest productRequest = productRequestService.getProductRequest(responseDTO.getId());

        mvc.perform(MockMvcRequestBuilders
                .delete("/rest/productrequest/api/deleteProductRequest/" + Integer.toString(productRequest.getId())))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

	}
	
	/**
     * Method to transform an object into a Json object. Used to give json as Request Body in requests.
     * @param obj : the object to transform in Json String.
     * @return
     */
    public static String asJsonString(Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
	
    /**
     * Method to transform a Json object into a ArticleDTO
     * @param json : the json string to transorm into a ArticleDTO
     * @return
     */
	public static ProductRequestDTO jSonAsProductRequest(String json) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			//JSON string to Java Object
			ProductRequestDTO cbDTO = mapper.readValue(json, ProductRequestDTO.class);
			return cbDTO;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
     * Convert a ProductRequest into a ProductRequestDTO
     * @param productRequest
     * @return
     */
    public ProductRequestDTO productRequestToProductRequestDTO(ProductRequest productRequest) {
        ProductRequestDTO productRequestDTO = new ProductRequestDTO(productRequest.getId(), productRequest.getUserId(), 
        		productRequest.getStartDate(), productRequest.getEndDate(),productRequest.getProduct());

        return productRequestDTO;
    }

}

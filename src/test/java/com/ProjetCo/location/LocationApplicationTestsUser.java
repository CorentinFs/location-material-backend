package com.ProjetCo.location;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import com.ProjetCo.location.controller.UserController;
import com.ProjetCo.location.dto.UserDTO;
import com.ProjetCo.location.model.User;
import com.ProjetCo.location.service.UserServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(UserController.class)
@Transactional
public class LocationApplicationTestsUser {
	@Autowired
	private MockMvc mvc;
	
	@Autowired
	UserServiceImpl serviceImpl;
	
	@Test
	public void createAndDeleteAdministrator() throws Exception
	{
		/* Create the administrator */
		long userId = 356012453;
		
		/* Get the User from the dataBase */
		User user = serviceImpl.getUser(userId);
		UserDTO userDTO = userToDTO(user);
		
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
				.post("/rest/user/api/createAdministrator")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(userDTO))
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isCreated())
            	.andReturn();
		
		UserDTO userDTOResult = jSonAsUser(mvcResult.getResponse().getContentAsString());
		
		assertEquals(userDTOResult.isAdmin(), true);
		
		/* Delete the Administrator */
		MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders
				.post("/rest/user/api/deleteAdministrator")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(userDTOResult))
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isCreated())
            	.andReturn();
		
		UserDTO userDTOResultDelete = jSonAsUser(mvcResult2.getResponse().getContentAsString());
		
		assertEquals(userDTOResultDelete.isAdmin(), false);
	}
	
	@Test
	public void getUserByUserId() throws Exception {
		//Get a user of the db (data populate)
		User user = serviceImpl.getUser(356012453l);
		UserDTO userDTO = userToDTO(user);
		String jSon = asJsonString(userDTO);
		
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .get("/rest/user/api/getUserByUserId")
                .param("userId", Long.toString(356012453)))
                .andReturn();
		
		assertEquals(mvcResult.getResponse().getContentAsString(), jSon);
	}
	
	/**
     * Method to transform a Json object into a UserDTO
     * @param json : the json string to transorm into a UserDTO
     * @return
     */
	public static UserDTO jSonAsUser(String json) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			//JSON string to Java Object
			UserDTO userDTO = mapper.readValue(json, UserDTO.class);
			return userDTO;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
     * Method to transform an object into a Json object. Used to give json as Request Body in requests.
     * @param obj : the object to transform in Json String.
     * @return
     */
    public static String asJsonString(Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
	
	/**
	 * Transform a User object into a UserDTO object.
	 * @param user : the user to transform.
	 */
	public UserDTO userToDTO(User user)
	{
		UserDTO res = new UserDTO(user.getId(), user.getUserId(), user.getPassword(), user.isAdmin(), user.isSuperAdmin());
		return res;
	}
	
}

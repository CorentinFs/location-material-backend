package com.ProjetCo.location.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Article {	
	private int id;
	private String name;
	private String description;
	
	private Category category;
	@JsonIgnore
	private List<Product> products;
	private byte[] image;
	
	public Article() {}
	
	public Article(String name, String description, Category category, List<Product> products, byte[] image) {
		super();
		this.name = name;
		this.description = description;
		this.category = category;
		this.products = products;
		this.image = image;
	}
	
	@Id	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	public int getId() {
		return id;
	}
	
	@Column(name="NAME", nullable=false)
	public String getName() {
		return name;
	}
	
	@Column(name="DESCRIPTION", nullable=false)
	public String getDescription() {
		return description;
	}
	
	@Lob 
	@Column(name="IMAGE", columnDefinition="BLOB")
	public byte[] getImage() {
		return image;
	}
	
	@ManyToOne
	@JoinColumn(name="CATEGORY", referencedColumnName = "ID")
	public Category getCategory() {
		return category;
	}
	
	@JsonIgnore
	@OneToMany(mappedBy="article",
			cascade = CascadeType.ALL,
	        orphanRemoval = true,
	        fetch = FetchType.LAZY)
	public List<Product> getProducts(){
		return products;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}	
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setCategory(Category category) {
		this.category = category;
	}
	
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	public void setImage(byte[] image) {
		this.image = image;
	}
}

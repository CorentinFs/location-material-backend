package com.ProjetCo.location.model;

import java.time.LocalDate;
import javax.persistence.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
public class ProductRequest {
	private int id;
	private long userId;

	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate startDate;
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate endDate;

	private Product product;
	
	public ProductRequest() {}
	
	public ProductRequest(long userId, LocalDate sd, LocalDate ed, Product product) {
		super();
		this.userId = userId;
		startDate = sd;
		endDate = ed;
		this.product = product;
	}


	@Id	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	public int getId() {
		return id;
	}
	
	@Column(name="USERID", nullable=false)
	public long getUserId() {
		return userId;
	}
	
	@Column(name="STARTDATE", nullable=false)
	public LocalDate getStartDate() {
		return startDate;
	}
	
	@Column(name="ENDDATE", nullable=false)
	public LocalDate getEndDate() {
		return endDate;
	}

	@ManyToOne
	@JoinColumn(name="PRODUCT", referencedColumnName = "ID")
	public Product getProduct() {
		return product;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}
	
	public void setProduct(Product product) {
		this.product = product;
	}	
}

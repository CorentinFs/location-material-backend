package com.ProjetCo.location.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
public class ConsumableBorrowing {
	private int id; 
	private String name;
	private long userId;
	
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate startDate;
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate endDate;
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate renderDate;
	
	public ConsumableBorrowing() {}
	
	public ConsumableBorrowing(String name, long userId, LocalDate sd, LocalDate ed, LocalDate rd) {
		super();
		this.name = name;
		this.userId = userId;
		startDate = sd;
		endDate = ed;
		renderDate = rd;
	}

    public ConsumableBorrowing(String name, long userId, LocalDate sd, LocalDate ed) {
		this.name = name;
		this.userId = userId;
		startDate = sd;
		endDate = ed;
    }

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	public int getId() {
		return id;
	}
	
	@Column(name="NAME", nullable=false)
	public String getName() {
		return name;
	}
	
	@Column(name="USERID", nullable=false) 
	public long getUserId() {
		return userId;
	}
	
	@Column(name="STARTDATE", nullable=false) 
	public LocalDate getStartDate() {
		return startDate;
	}
	
	@Column(name="ENDDATE", nullable=false) 
	public LocalDate getEndDate() {
		return endDate;
	}
	
	@Column(name="RENDERDATE", nullable = true) 
	public LocalDate getRenderDate() {
		return renderDate;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}
	
	public void setRenderDate(LocalDate renderDate) {
		this.renderDate = renderDate;
	}
}

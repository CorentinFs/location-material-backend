package com.ProjetCo.location.model;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
public class ProductBorrowing {
	private int id;
	private long userId;

	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate startDate;
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate endDate;
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate renderDate;

	private String adminComment;
	private boolean pickedUp;
	private Product product;
	
	public ProductBorrowing() {}
	
	public ProductBorrowing(long userId, LocalDate sd, LocalDate ed, LocalDate rd, String adminC, boolean pickedUp, Product product) {
		super();
		this.userId = userId;
		startDate = sd;
		endDate = ed;
		renderDate = rd;
		adminComment = adminC;
		this.pickedUp = pickedUp;
		this.product = product;
	}

    public ProductBorrowing(long userId, LocalDate sd, LocalDate ed, String adminC, Product product) {
		this.userId = userId;
		startDate = sd;
		endDate = ed;
		adminComment = adminC;
		this.product = product;
    }

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	public int getId() {
		return id;
	}
	
	@Column(name="USERID", nullable=false)
	public long getUserId() {
		return userId;
	}
	
	@Column(name="STARTDATE", nullable=false)
	public LocalDate getStartDate() {
		return startDate;
	}
	
	@Column(name="ENDDATE", nullable=false)
	public LocalDate getEndDate() {
		return endDate;
	}
	
	@Column(name="RENDERDATE", nullable=true)
	public LocalDate getRenderDate() {
		return renderDate;
	}
	
	@Column(name="ADMINCOMMENT", nullable=true)
	public String getAdminComment() {
		return adminComment;
	}
	
	@Column(name="PICKEDUP", nullable=false)
	public boolean getPickedUp() {
		return pickedUp;
	}
	
	@ManyToOne
	@JoinColumn(name="PRODUCT", referencedColumnName = "ID")
	public Product getProduct() {
		return product;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public void setRenderDate(LocalDate renderDate) {
		this.renderDate = renderDate;
	}

	public void setAdminComment(String adminComment) {
		this.adminComment = adminComment;
	}

	public void setPickedUp(boolean pickedUp) {
		this.pickedUp = pickedUp;
	}

	public void setProduct(Product product) {
		this.product = product;
	}	
}

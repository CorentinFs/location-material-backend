package com.ProjetCo.location.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
public class ConsumableRequest {
	private int id; 
	private String name;
	private long userId;

	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate startDate;
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate endDate;
	
	public ConsumableRequest() {}
	
	public ConsumableRequest(String name, long userId, LocalDate sd, LocalDate ed) {
		super();
		this.name = name;
		this.userId = userId;
		startDate = sd;
		endDate = ed;
	}
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID", nullable=false)
	public int getId() {
		return id;
	}
	
	@Column(name="NAME", nullable=false)
	public String getName() {
		return name;
	}
	
	@Column(name="USERID", nullable=false) 
	public long getUserId() {
		return userId;
	}
	
	@Column(name="STARTDATE", nullable=false) 
	public LocalDate getStartDate() {
		return startDate;
	}
	
	@Column(name="ENDDATE", nullable=false) 
	public LocalDate getEndDate() {
		return endDate;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}
}

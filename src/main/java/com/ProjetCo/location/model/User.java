package com.ProjetCo.location.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User {
	private int id;
	private long userId;
	private String password;
	private boolean isAdmin;
	private boolean isSuperAdmin;
	
	public User() {};
	//TODO ajouter une liste de consumableRequest et productRequest  pour un utilisateur
	public User(long userId, String password, boolean isAdmin, boolean isSuperAdmin) {
		this.userId = userId;
		this.password = password;
		this.isAdmin = isAdmin;
		this.isSuperAdmin = isSuperAdmin;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	public int getId() {
		return id;
	}
	
	@Column(name="USERID", nullable=false)
	public long getUserId() {
		return userId;
	}
	
	@Column(name="PASSWORD", nullable=false)
	public String getPassword() {
		return password;
	}
	
	@Column(name="ISADMIN", nullable=false)
	public boolean isAdmin() {
		return isAdmin;
	}
	
	@Column(name="ISSUPERADMIN", nullable=false)
	public boolean isSuperAdmin() {
		return isSuperAdmin;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public void setSuperAdmin(boolean isSuperAdmin) {
		this.isSuperAdmin = isSuperAdmin;
	}
 }

package com.ProjetCo.location.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Category {	
	private int id;
	private String name;
	
	@JsonIgnore
	private List<Article> articles;
	
	public Category() {}
	
	public Category(String name, List<Article> articles) {
		//super();
		this.name = name;
		this.articles = articles;
	}
	
	public Category(int id, String name) {
		this.id = id;
		this.name = name;
		articles = new ArrayList<Article>();
	}
	
	@Id	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	public int getId() {
		return id;
	}
	
	@Column(name="NAME", nullable=false)
	public String getName() {
		return name;
	}
	
	@OneToMany(mappedBy="category",
			cascade = CascadeType.ALL,
	        orphanRemoval = true,
	        fetch=FetchType.LAZY)
	public List<Article> getArticles(){
		return articles;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}
}

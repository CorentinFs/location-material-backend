package com.ProjetCo.location.model;

import javax.persistence.*;

@Entity
public class Product {
	private int id;
	private double barcode; 
	private String state;
	private boolean available;
	private boolean reserved;
	
	private boolean tp;
	private boolean donation;
	private boolean hs;
	
	private Article article;

	public Product() {}
	
	public Product(double barcode, String state, boolean isAvailable, boolean isReserved, Article article) {
		super();
		this.barcode = barcode;
		this.state = state;
		this.available = isAvailable;
		this.reserved = isReserved;
		this.article = article;
		tp = false;
		donation = false;
		hs = false;
	}
	
	public Product(double barcode, String state, boolean isAvailable, boolean isReserved, Article article, boolean tp, boolean donation, boolean hs) {
		super();
		this.barcode = barcode;
		this.state = state;
		this.available = isAvailable;
		this.reserved = isReserved;
		this.article = article;
		this.tp = tp;
		this.donation = donation;
		this.hs = hs;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	public int getId() {return id;}

	@Column(name="BARCODE", nullable=false)
	public double getBarcode() {
		return barcode;
	}
	
	@Column(name="STATE")
	public String getState() {
		return state;
	}
	
	@Column(name="ISAVAILABLE", nullable=false)
	public boolean isAvailable() {
		return available;
	}
	
	@Column(name="ISRESERVED", nullable=false)
	public boolean isReserved() {
		return reserved;
	}
	
	@Column(name="HS", nullable = false)
	public boolean isHs() {
		return hs;
	}
	
	@Column(name="TP", nullable = false)
	public boolean isTp() {
		return tp;
	}
	
	@Column(name="DONATION", nullable = false)
	public boolean isDonation() {
		return donation;
	}
	
	@ManyToOne
	@JoinColumn(name="ARTICLE", referencedColumnName = "ID")
	public Article getArticle() {
		return article;
	}

	public void setId(int id) {this.id = id;}

	public void setBarcode(double barcode) {
		this.barcode = barcode;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public void setAvailable(boolean isAvailable) {
		this.available = isAvailable;
	}

	public void setReserved(boolean isReserved) {
		this.reserved = isReserved;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public void setDonation(boolean donation) {
		this.donation = donation;
	}

	public void setTp(boolean tp) {
		this.tp = tp;
	}

	public void setHs(boolean hs) {
		this.hs = hs;
	}
}

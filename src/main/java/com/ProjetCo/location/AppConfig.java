package com.ProjetCo.location;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ProjetCo.location.service.ArticleServiceImpl;
import com.ProjetCo.location.service.CategoryServiceImpl;

@Configuration
public class AppConfig {
	@Bean
	public CategoryServiceImpl categoryServiceImpl() {
		return new CategoryServiceImpl();
	}
	
	@Bean
	public ArticleServiceImpl articleServiceImpl() {
		return new ArticleServiceImpl();
	}
}

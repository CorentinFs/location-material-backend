package com.ProjetCo.location.service;

import java.util.List;
import java.util.Optional;

import com.ProjetCo.location.model.Article;

public interface ArticleService {
	public Article saveArticle(Article article);
	
	public Optional<Article> getArticle(int id);
	
	public Article getArticle(String name);
	
	public void deleteArticle(int id);

	public boolean existsArticle(String name);

	public boolean existsArticle(int id);

	public List<Article> findAll();
}

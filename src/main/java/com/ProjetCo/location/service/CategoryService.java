package com.ProjetCo.location.service;

import java.util.List;
import java.util.Optional;

import com.ProjetCo.location.model.Category;

public interface CategoryService {
	public Category saveCategory(Category category);
	
	public Category getCategory(String name);
	
	public void deleteCategory(int categoryId);
	
	public boolean existsCategory(String name);
	
	public boolean existsCategory(int id);
	
	public List<Category> findAll();
	
	public Optional<Category> findById(int categoryId);
}	

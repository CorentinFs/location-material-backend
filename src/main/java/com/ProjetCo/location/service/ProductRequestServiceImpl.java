package com.ProjetCo.location.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ProjetCo.location.model.ProductRequest;
import com.ProjetCo.location.repository.ProductRequestDAO;

@Service("productRequestService")
@Transactional
public class ProductRequestServiceImpl implements ProductRequestService{
	@Autowired
	public ProductRequestDAO productRequestDAO;
	
	@Override
	public List<ProductRequest> getProductRequestsByUserId(long userId) {
		return productRequestDAO.getProductRequestByUserId(userId);
	}
	
	@Override
	public ProductRequest saveProductRequest(ProductRequest productRequest) {
		return productRequestDAO.save(productRequest);
	}
	
	@Override
	public List<ProductRequest> getAllProductRequest(){
		return productRequestDAO.findAll();
	}
	
	@Override
	public ProductRequest getProductRequest(int id) {
		return productRequestDAO.getProductRequest(id);
	}

	@Override
	public void deleteProductRequest(int id) {
		productRequestDAO.deleteProductRequest(id);
	}

	@Override
	public boolean existsIdProductRequest(int id) { 
		return productRequestDAO.existsProductRequest(id) != null; 
	}

	@Override
	public List<ProductRequest> findAll(){
		return productRequestDAO.findAll();
	}

}

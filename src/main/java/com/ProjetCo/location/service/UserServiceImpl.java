package com.ProjetCo.location.service;

import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ProjetCo.location.model.User;
import com.ProjetCo.location.repository.UserDAO;

@Service("UserService")
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
	public UserDAO UserDAO;
	
	@Override
	public User saveUser(User User) {
		return UserDAO.save(User);
	}
	
	@Override
	public User getUser(long userId) {
		return UserDAO.getUser(userId);
	}
	
	@Override
	public Optional<User> getUser(int id) {
		return UserDAO.findById(id);
	}
	
	@Override
	public void deleteUser(int UserId) {
		UserDAO.deleteById(UserId);
	}
	
	@Override
	public boolean existsUser(long userId) {
		return (UserDAO.getUser(userId) != null);
	}
	
	@Override
	public boolean existsUser(int id) {
		return UserDAO.existsUser(id);
	}
	
	@Override
	public List<User> findAll() {
		return UserDAO.findAll();
	}
	
	@Override
	public boolean connect(long userId, String password) {
		return UserDAO.connectUser(userId, password);
	}
}

package com.ProjetCo.location.service;

import java.util.List;
import java.util.Optional;

import com.ProjetCo.location.model.User;

public interface UserService {	
	public User saveUser(User User);
	
	public User getUser(long userId);
	
	public void deleteUser(int UserId);
	
	public boolean existsUser(long userId);
	
	public boolean existsUser(int id);
	
	public List<User> findAll();
	
	public boolean connect(long userId, String password);
	
	public Optional<User> getUser(int id);
}

package com.ProjetCo.location.service;

import java.util.List;

import com.ProjetCo.location.model.Article;
import com.ProjetCo.location.model.Product;

public interface ProductService {
	public Product saveProduct(Product product);

	public Product getProduct(double barcode);
	
	public void deleteProduct(double barcode);
	
	public List<Product> findAll();
	
	public boolean existsProduct(double barcode);
	
	public List<Product> getProductsOfArticle(Article article);
}

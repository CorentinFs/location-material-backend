package com.ProjetCo.location.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ProjetCo.location.model.Category;
import com.ProjetCo.location.repository.CategoryDAO;

@Service("categoryService")
@Transactional
public class CategoryServiceImpl implements CategoryService{
	
	@Autowired
    private CategoryDAO categoryDao;
	
	@Override
	public Category saveCategory(Category category) {
		return categoryDao.save(category);
	}
	
	@Override
	public Category getCategory(String name) {
		return categoryDao.getCategory(name);
	}
	
	@Override
	public void deleteCategory(int categoryId) {
		categoryDao.deleteById(categoryId);
	}
	
	@Override
	public boolean existsCategory(String name) {
		return (categoryDao.getCategory(name) != null);
	}
	
	@Override
	public boolean existsCategory(int id) {
		return categoryDao.existsCategory(id);
	}
	
	@Override
	public List<Category> findAll(){
		return categoryDao.findAll();
	}
	
	@Override
	public Optional<Category> findById(int categoryId) {
		return categoryDao.findById(categoryId);
	}
}

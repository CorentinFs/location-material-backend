package com.ProjetCo.location.service;

import java.util.List;

import com.ProjetCo.location.model.ProductBorrowing;
import com.ProjetCo.location.model.ProductRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ProjetCo.location.model.Article;
import com.ProjetCo.location.model.Product;
import com.ProjetCo.location.repository.ProductDAO;

@Service("productService")
@Transactional
public class ProductServiceImpl implements ProductService{
	@Autowired
	public ProductDAO productDAO;
	
	@Override
	public List<Product> getProductsOfArticle(Article article){
		return productDAO.getProductsOfArticle(article);
	}
	
	@Override
	public Product saveProduct(Product product) {
		return productDAO.save(product);
	}
	
	@Override
	public List<Product> findAll(){
		return productDAO.findAll();
	}
	
	@Override
	public Product getProduct(double barcode) {
		return productDAO.getProduct(barcode);
	}
	
	@Override
	public void deleteProduct(double barcode) {
		productDAO.deleteProduct(barcode);
	}

	@Override
	public boolean existsProduct(double barcode) { 
		return (productDAO.getProduct(barcode) != null); 
	}

	public List<ProductRequest> getAllProductRequest(double barcode) { return productDAO.getAllProductRequest(barcode);}

	public List<ProductBorrowing> getAllProductBorrowing(double barcode) { return productDAO.getAllProductBorrowing(barcode);}

}

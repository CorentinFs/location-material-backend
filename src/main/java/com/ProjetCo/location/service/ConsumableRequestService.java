package com.ProjetCo.location.service;

import java.time.LocalDate;
import java.util.List;

import com.ProjetCo.location.model.ConsumableRequest;

public interface ConsumableRequestService {
	public ConsumableRequest saveConsumableRequest(ConsumableRequest consumableRequest);
	
	public ConsumableRequest getConsumableRequest(int id);
	
	public void deleteConsumableRequest(int consumableRequestId);
	
	public boolean existsConsumableRequest(String name, long userId, LocalDate startDate, LocalDate endDate);
	
	public boolean existsConsumableRequest(int id);
	
	public List<ConsumableRequest> findAll();
	
	public List<ConsumableRequest> getConsumableRequestsByUserId(long userId);
}


package com.ProjetCo.location.service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import com.ProjetCo.location.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ProjetCo.location.model.ProductBorrowing;
import com.ProjetCo.location.repository.ProductBorrowingDAO;

@Service("productBorrowingService")
@Transactional
public class ProductBorrowingServiceImpl implements ProductBorrowingService {
	@Autowired
	public ProductBorrowingDAO productBorrowingDAO;
	
	@Override
	public List<ProductBorrowing> getNotAvailableProductBorrowing(Product product, LocalDate today){
		return productBorrowingDAO.getNotAvailableProductBorrowing(product, today);
	}
	
	@Override
	public ProductBorrowing saveProductBorrowing(ProductBorrowing productBorrowing) {
		return productBorrowingDAO.save(productBorrowing);
	}
	
	 @Override
	 public List<ProductBorrowing> getAllProductBorrowing(){
		 return productBorrowingDAO.findAll();
	 }

	 @Override
	 public List<ProductBorrowing> getProductBorrowingByUserId(long id) {
		return productBorrowingDAO.getProductBorrowingByUserId(id);
	 }
	 @Override
	 public ProductBorrowing getProductBorrowing(int id) {
		 return productBorrowingDAO.getProductBorrowing(id);
	 }
	 
	 @Override
	 public void deleteProductBorrowing(int id) {
		 productBorrowingDAO.deleteProductBorrowing(id);
	 }
	 
	 @Override
	 public boolean existsProductBorrowing(int id) {
		 return productBorrowingDAO.findById(id) != null;
	 }

	 @Override
	 public boolean existsProductBorrowing(long userId, Date sd, Date ed, Date rd, String adminC, boolean pickedUp, Product product) { 
		 return (productBorrowingDAO.getIdProductBorrowing(userId,sd,ed,rd,adminC,pickedUp,product) != null); 
	 }

	 @Override
	 public boolean existsIdProductBorrowing(int id) {
		 return productBorrowingDAO.existsById(id);
	 }

	 @Override
     public List<ProductBorrowing> getReturnedProductBorrowingByUserId(long id) {
    	 return productBorrowingDAO.getReturnedProductBorrowingByUserId(id);
	 }

	 @Override
    public ProductBorrowing getProductBorrowingWithParam(long userId, LocalDate sd, LocalDate ed, int productId){
		return productBorrowingDAO.getProductBorrowingWithParam(userId, productId, sd, ed);
    }
}

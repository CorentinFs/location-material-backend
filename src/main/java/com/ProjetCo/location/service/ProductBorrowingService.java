package com.ProjetCo.location.service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import com.ProjetCo.location.model.Product;
import com.ProjetCo.location.model.ProductBorrowing;

public interface ProductBorrowingService {
	public ProductBorrowing saveProductBorrowing(ProductBorrowing productBorrowing);
	
	public List<ProductBorrowing> getAllProductBorrowing();

	public ProductBorrowing getProductBorrowing(int id);
	
	public List<ProductBorrowing> getProductBorrowingByUserId(long id);
	
	public boolean existsProductBorrowing(long userId, Date sd, Date ed, Date rd, String adminC, boolean pickedUp, Product product);

	public boolean existsIdProductBorrowing(int id);
	
	public List<ProductBorrowing> getReturnedProductBorrowingByUserId(long id);
	
	void deleteProductBorrowing(int id);
	
	public boolean existsProductBorrowing(int id);
	
	public List<ProductBorrowing> getNotAvailableProductBorrowing(Product product, LocalDate today);
	
	public ProductBorrowing getProductBorrowingWithParam(long userId, LocalDate sd, LocalDate ed, int productId);
}

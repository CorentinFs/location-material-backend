package com.ProjetCo.location.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ProjetCo.location.model.ConsumableRequest;
import com.ProjetCo.location.repository.ConsumableRequestDAO;

@Service("consumableRequestService")
@Transactional
public class ConsumableRequestServiceImpl implements ConsumableRequestService{
	
	@Autowired
	public ConsumableRequestDAO consumableRequestDAO;
	
	@Override
	public List<ConsumableRequest> getConsumableRequestsByUserId(long userId) {
		return consumableRequestDAO.getConsumableRequestByUserId(userId);
	}
	
	@Override
	public ConsumableRequest saveConsumableRequest(ConsumableRequest consumableRequest) {
		return consumableRequestDAO.save(consumableRequest);
	}
	
	@Override
	public ConsumableRequest getConsumableRequest(int id) {
		return consumableRequestDAO.getConsumableRequest(id);
	}
	
	@Override
	public void deleteConsumableRequest(int consumableRequestId) {
		consumableRequestDAO.deleteById(consumableRequestId);
	}
	
	@Override
	public boolean existsConsumableRequest(String name, long userId, LocalDate startDate, LocalDate endDate) {
		return (consumableRequestDAO.existsConsumableRequest(name, userId, startDate, endDate) != null);
	}
	
	@Override
	public boolean existsConsumableRequest(int id) {
		return consumableRequestDAO.existsConsumableRequest(id);
	}

	@Override
	public List<ConsumableRequest> findAll() {
		return consumableRequestDAO.findAll();
	}
}

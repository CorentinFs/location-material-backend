package com.ProjetCo.location.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ProjetCo.location.model.ConsumableBorrowing;
import com.ProjetCo.location.repository.ConsumableBorrowingDAO;

@Service("consumableBorrowingService")
@Transactional
public class ConsumableBorrowingServiceImpl implements ConsumableBorrowingService {
	
	@Autowired
	public ConsumableBorrowingDAO consumableBorrowingDAO;
	
	@Override
	public ConsumableBorrowing saveConsumableBorrowing(ConsumableBorrowing consumableBorrowing) {
		return consumableBorrowingDAO.save(consumableBorrowing);
	}
	
	@Override
	public ConsumableBorrowing getConsumableBorrowing(int id) {
		return consumableBorrowingDAO.getConsumableBorrowing(id);
	}
	
	@Override
	public void deleteConsumableBorrowing(int consumableBorrowingId) {
		consumableBorrowingDAO.deleteById(consumableBorrowingId);
	}
	
	@Override
	public boolean existsConsumableBorrowing(String name, long userId, LocalDate startDate, LocalDate endDate, LocalDate renderDate) {
		return (consumableBorrowingDAO.existsConsumableBorrowing(name, userId, startDate, endDate, renderDate) != null);
	}
	
	@Override
	public boolean existsConsumableBorrowing(int id) {
		return consumableBorrowingDAO.existsConsumableBorrowing(id);
	}
	
	@Override
	public List<ConsumableBorrowing> findAll() {
		return consumableBorrowingDAO.findAll();
	}
	
	@Override
    public List<ConsumableBorrowing> getConsumableBorrowingByUserId(long id) {
		return consumableBorrowingDAO.getConsumableBorrowingByUserId(id);
    }

	@Override
	public List<ConsumableBorrowing> getReturnedConsumableBorrowingByUserId(long id) {
		return consumableBorrowingDAO.getReturnedConsumableBorrowingByUserId(id);
	}
}

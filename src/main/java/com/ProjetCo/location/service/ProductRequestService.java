package com.ProjetCo.location.service;

import java.util.List;

import com.ProjetCo.location.model.ProductRequest;

public interface ProductRequestService {
	public ProductRequest saveProductRequest(ProductRequest productRequest);
	
	public List<ProductRequest> getAllProductRequest();
	
	public ProductRequest getProductRequest(int id);
	
	public void deleteProductRequest(int id);
	
	public List<ProductRequest> getProductRequestsByUserId(long userId);
	
	public boolean existsIdProductRequest(int id);
	
	public List<ProductRequest> findAll();
}

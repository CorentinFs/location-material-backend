package com.ProjetCo.location.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ProjetCo.location.model.Article;
import com.ProjetCo.location.repository.ArticleDAO;

@Service("articleService")
@Transactional
public class ArticleServiceImpl implements ArticleService{
	@Autowired
	public ArticleDAO articleDao;
	
	@Override
	public Article saveArticle(Article article) {
		return articleDao.save(article);
	}
	
	@Override
	public Article getArticle(String name){
		return articleDao.getArticle(name);
	}
	
	@Override
	public Optional<Article> getArticle(int id) {
		return articleDao.findById(id);
	}

	@Override
	public void deleteArticle(int id) { 
		articleDao.deleteById(id); 
	}

	@Override
	public boolean existsArticle(String name) {
		return (articleDao.getArticle(name) != null);
	}

	@Override
	public boolean existsArticle(int id) {
		return articleDao.existsArticle(id);
	}

	@Override
	public List<Article> findAll(){
		return articleDao.findAll();
	}
}

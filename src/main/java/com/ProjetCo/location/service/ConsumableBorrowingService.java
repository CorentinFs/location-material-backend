package com.ProjetCo.location.service;

import java.time.LocalDate;
import java.util.List;

import com.ProjetCo.location.model.ConsumableBorrowing;

public interface ConsumableBorrowingService {
	public ConsumableBorrowing saveConsumableBorrowing(ConsumableBorrowing consumableBorrowing);
	
	public ConsumableBorrowing getConsumableBorrowing(int id);
	
	public void deleteConsumableBorrowing(int consumableBorrowingId);
	
	public boolean existsConsumableBorrowing(String name, long userId, LocalDate startDate, LocalDate endDate, LocalDate renderDate);
	
	public boolean existsConsumableBorrowing(int id);
	
	public List<ConsumableBorrowing> findAll();
	
	public List<ConsumableBorrowing> getConsumableBorrowingByUserId(long id);
	
	public List<ConsumableBorrowing> getReturnedConsumableBorrowingByUserId(long id);
}

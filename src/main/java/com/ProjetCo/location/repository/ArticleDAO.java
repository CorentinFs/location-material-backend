package com.ProjetCo.location.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ProjetCo.location.model.Article;

@Repository
public interface ArticleDAO extends JpaRepository<Article, Integer>{	
    /**
     * Select a specific Article.
     * @param name : the name of the Article to select.
     * @return Article : the Article object corresponding to the name.
     */
    @Query("SELECT A FROM Article A WHERE A.name = ?1")
    public Article getArticle(String name);

    /**
     * Check if a Category already exists by its name.
     * @param name : the name of the Category to check.
     * @return boolean : Category exists.
     */
    @Query("SELECT count(A)>0 FROM Article A WHERE A.name = ?1")
    public boolean existsArticle(String name);

    /**
     * Check if a Category already exists by its ID.
     * @param name : the name of the Category to check.
     * @return boolean : Category exists.
     */
    @Query("SELECT count(A)>0 FROM Article A WHERE A.id = ?1")
    public boolean existsArticle(int id);
}

package com.ProjetCo.location.repository;

import com.ProjetCo.location.model.ProductBorrowing;
import com.ProjetCo.location.model.ProductRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ProjetCo.location.model.Article;
import com.ProjetCo.location.model.Product;

import java.util.List;

@Repository
public interface ProductDAO extends JpaRepository<Product, Double> {
	
	/**
	 * Get the List of Product related to a given Article id.
	 * @param article : the article.
	 * @return List<Product> : the list of all the related Products.
	 */
	@Query("SELECT P FROM Product P WHERE P.article=?1")
	public List<Product> getProductsOfArticle(Article article);
	
    /**
     * Select all the Products.
     * @return List<Product> : the list of all Products.
     */
    @Query("SELECT P FROM Product P")
    public List<Product> getAllProduct();

    /**
     * Select a specific Product.
     * @param barcode : the barcode of the Product to select.
     * @return Product : the Product object corresponding to the barcode.
     */
    @Query("SELECT P FROM Product P WHERE P.barcode = ?1")
    public Product getProduct(double barcode);

    /**
     * Delete a specific Product.
     * @param barcode : the barcode of the Product to delete.
     */
    @Modifying
    @Query("DELETE FROM Product P WHERE P.barcode = ?1")
    public void deleteProduct(double barcode);

    @Query("SELECT PR FROM ProductRequest PR INNER JOIN Product P ON PR.product.id = P.id WHERE P.barcode = ?1")
    List<ProductRequest> getAllProductRequest(double barcode);

    @Query("SELECT PB FROM ProductBorrowing PB INNER JOIN Product P ON PB.product.id = P.id WHERE P.barcode = ?1")
    List<ProductBorrowing> getAllProductBorrowing(double barcode);
}

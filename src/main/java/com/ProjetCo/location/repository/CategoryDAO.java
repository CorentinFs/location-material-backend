package com.ProjetCo.location.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.ProjetCo.location.model.Category;

@Repository
public interface CategoryDAO extends JpaRepository<Category, Integer>{	
	/**
	 * Select a specific Category.
	 * @param name : the name of the Category to select.
	 * @return Category : the Category Object corresponding to the name.
	 */
	@Query("SELECT C FROM Category C WHERE C.name = ?1")
	public Category getCategory(String name);
	
	/**
	 * Check if a Category already exists by its name.
	 * @param name : the name of the Category to check.
	 * @return boolean : Category exists.
	 */
	@Query("SELECT count(C)>0 FROM Category C WHERE C.name = ?1")
	public boolean existsCategory(String name);
	
	/**
	 * Check if a Category already exists by its ID.
	 * @param name : the name of the Category to check.
	 * @return boolean : Category exists.
	 */
	@Query("SELECT count(C)>0 FROM Category C WHERE C.id = ?1")
	public boolean existsCategory(int id);
}

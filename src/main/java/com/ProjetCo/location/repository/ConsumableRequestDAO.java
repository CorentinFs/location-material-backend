package com.ProjetCo.location.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ProjetCo.location.model.ConsumableRequest;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ConsumableRequestDAO extends JpaRepository<ConsumableRequest, Integer> {
	
	/**
	 * Get the List of all ConsumableRequest related to a specific userId.
	 * @param userId : the userId of the User.
	 * @return List<ConsumableRequest> : the List of ConsumableRequests related to the User.
	 */
	@Query("SELECT Cr FROM ConsumableRequest Cr WHERE Cr.userId = ?1")
	public List<ConsumableRequest> getConsumableRequestByUserId(long userId);
	
    /**
     * Select a specific ConsumableRequest.
     * @param id : the id of the ConsumableRequest to select.
     * @return ConsumableRequest : the ConsumableRequest object corresponding to the id.
     */
    @Query("SELECT CR FROM ConsumableRequest CR WHERE CR.id = ?1")
    public ConsumableRequest getConsumableRequest(int id);

    /**
     * Check if a ConsumableRequest exists in the DB
     * @param name : name of the ConsumableRequest to check
     * @param userId : userId of the ConsumableRequest to check
     * @param startDate : startDate of the ConsumableRequest to check
     * @param endDate : endDate of the ConsumableRequest to check
     * @return 
     */
    @Query("SELECT CR FROM ConsumableRequest CR WHERE CR.name = ?1 AND (CR.userId = ?2 AND (CR.startDate = ?3 AND CR.endDate = ?4))")
    public ConsumableRequest existsConsumableRequest(String name, long userId, LocalDate startDate, LocalDate endDate);
    
    /**
	 * Check if a ConsumableRequest already exists by its ID.
	 * @param id : the id of the ConsumableRequest to check.
	 * @return boolean : ConsumableRequest exists.
	 */
	@Query("SELECT count(CR)>0 FROM ConsumableRequest CR WHERE CR.id = ?1")
	public boolean existsConsumableRequest(int id);
}

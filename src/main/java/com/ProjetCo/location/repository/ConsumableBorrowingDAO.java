package com.ProjetCo.location.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ProjetCo.location.model.ConsumableBorrowing;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ConsumableBorrowingDAO extends JpaRepository<ConsumableBorrowing, Integer> {
    /**
     * Select a specific ConsumableBorrowing.
     * @param id : the id of the ConsumableBorrowing to select.
     * @return ConsumableBorrowing : the ConsumableBorrowing object corresponding to the id.
     */
    @Query("SELECT CB FROM ConsumableBorrowing CB WHERE CB.id = ?1")
    public ConsumableBorrowing getConsumableBorrowing(int id);
 
    /**
     * Check if a ConsumableBorrowing exists in the DB
     * @param name : name of the ConsumableBorrowing to check
     * @param userId : userId of the ConsumableBorrowing to check
     * @param startDate : startDate of the ConsumableBorrowing to check
     * @param endDate : endDate of the ConsumableBorrowing to check
     * @param renderDate : renderDate of the ConsumableBorrowing to check
     * @return 
     */
    @Query("SELECT CB FROM ConsumableBorrowing CB WHERE CB.name = ?1 AND CB.userId = ?2 AND CB.startDate = ?3 AND CB.endDate = ?4 AND CB.renderDate = ?5")
    public ConsumableBorrowing existsConsumableBorrowing(String name, long userId, LocalDate startDate, LocalDate endDate, LocalDate renderDate);
    
    /**
	 * Check if a ConsumableBorrowing already exists by its ID.
	 * @param id : the id of the ConsumableBorrowing to check.
	 * @return boolean : ConsumableBorrowing exists.
	 */
	@Query("SELECT count(CB)>0 FROM ConsumableBorrowing CB WHERE CB.id = ?1")
	public boolean existsConsumableBorrowing(int id);

    @Query("SELECT CB FROM ConsumableBorrowing CB WHERE CB.userId = ?1")
    public List<ConsumableBorrowing> getConsumableBorrowingByUserId(long id);

    @Query("SELECT CB FROM ConsumableBorrowing CB WHERE CB.userId = ?1 AND CB.renderDate IS NOT NULL")
    public List<ConsumableBorrowing> getReturnedConsumableBorrowingByUserId(long id);
}

package com.ProjetCo.location.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ProjetCo.location.model.User;

@Repository
public interface UserDAO extends JpaRepository<User, Integer> {
    /**
     * Select a specific User.
     * @param userId : the userId of the User to select.
     * @return User : the User Object corresponding to the userId.
     */
    @Query("SELECT U FROM User U WHERE U.userId = ?1")
    public User getUser(long userId);
    
    /**
	 * Check if an User already exists by its ID.
	 * @param id : the id of the User to check.
	 * @return boolean : User exists.
	 */
	@Query("SELECT count(U)>0 FROM User U WHERE U.id = ?1")
	public boolean existsUser(int id);
	
	/**
	 Check if an User already exists by its ID.
	 * @param userId : the userId of the User to check.
	 * @param password : the password of the User to check.
	 * @return boolean : User exists.
	 */
	@Query("SELECT count(U)>0 FROM User U WHERE U.userId = ?1 AND U.password = ?2")
	public boolean connectUser(long userId, String password);
}

package com.ProjetCo.location.repository;

import com.ProjetCo.location.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.ProjetCo.location.model.ProductBorrowing;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface ProductBorrowingDAO extends JpaRepository<ProductBorrowing, Integer> {
	
	/**
	 * Select the ProductBorrowing related to the product id and for which the renderDate is later than today.
	 * @param productId : the id of the Product to select.
	 * @param today : the current date.
	 * @return List<ProductBorrowing> : the list of all ProductBorrowing.
	 */
	@Query("SELECT PB FROM ProductBorrowing PB WHERE PB.product = ?1 AND "
			+ "((DATEDIFF(?2, PB.endDate) <= 0) OR (DATEDIFF(?2, PB.endDate) >= 0 AND PB.renderDate IS NULL))")
	public List<ProductBorrowing> getNotAvailableProductBorrowing(Product product, LocalDate today);
	
    /**
     * Select all the ProductBorrowings.
     * @return List<ProductBorrowing> : the list of all ProductBorrowings.
     */
    @Query("SELECT PB FROM ProductBorrowing PB")
    public List<ProductBorrowing> getAllProductBorrowing();

    /**
     * Select a specific ProductBorrowing.
     * @param id : the id of the ProductBorrowing to select.
     * @return ProductBorrowing : the ProductBorrowing object corresponding to the id.
     */
    @Query("SELECT PB FROM ProductBorrowing PB WHERE PB.id = ?1")
    public ProductBorrowing getProductBorrowing(int id);

    /**
     * Delete a specific ProductBorrowing.
     * @param id : the id of the ProductBorrowing to delete.
     */
    @Modifying
    @Query("DELETE FROM ProductBorrowing PB WHERE PB.id = ?1")
    public void deleteProductBorrowing(int id);

    /**
     * Get all the ProductBorrowing from a user with its id
     * @param id the id of the user who made the request
     * @return List<ProductBorrowing> : return a list of ProductBorrowing corresponding to the given user id
     */
    @Query("SELECT PB FROM ProductBorrowing PB WHERE PB.userId = ?1")
    public List<ProductBorrowing> getProductBorrowingByUserId(long id);

    /**
     * Get the id of a ProductBorrowing
     */
    @Query("SELECT PB.id FROM ProductBorrowing PB WHERE PB.userId = ?1 " +
            "AND PB.startDate = ?2 " +
            "AND PB.endDate = ?3 " +
            "AND PB.renderDate = ?4 " +
            "AND PB.adminComment = ?5 " +
            "AND PB.pickedUp = ?6 " +
            "AND PB.product = ?7")
    public Integer getIdProductBorrowing(long userId, Date sd, Date ed, Date rd, String adminC, boolean pickedUp, Product product);

    @Query("SELECT PB FROM ProductBorrowing PB WHERE PB.userId = ?1 AND PB.renderDate IS NOT NULL")
    public List<ProductBorrowing> getReturnedProductBorrowingByUserId(long id);

    /**
     * Get a ProductBorrowing with userId, productId, startDate, endDate.
     * @param userId : the userId of the ProductBorrowing.
     * @param productId : the productId of the ProductBorrowing.
     * @param startDate : the startDate of the ProductBorrowing.
     * @param endDate : the endDate of the ProductBorrowing.
     * @return
     */
    @Query("SELECT PB FROM ProductBorrowing PB WHERE PB.userId = ?1 "
    		+ "AND PB.product.id = ?2 "
    		+ "AND PB.startDate = ?3 "
    		+ "AND PB.endDate = ?4")
    public ProductBorrowing getProductBorrowingWithParam(long userId, int productId, LocalDate startDate, LocalDate endDate);
}

package com.ProjetCo.location.repository;

import com.ProjetCo.location.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ProjetCo.location.model.ProductRequest;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ProductRequestDAO extends JpaRepository<ProductRequest, Integer> {
	/**
	 * Get the List of all the Product Requests done by a User.
	 * @param userId : the userId of the User who made the requests.
	 * @return List<ProductRequest> : all the ProductRequests of the User.
	 */
	@Query("SELECT Pr FROM ProductRequest Pr WHERE Pr.userId = ?1")
	public List<ProductRequest> getProductRequestByUserId(long userId);
	
    /**
     * Select a specific ProductRequest.
     * @param id : the id of the ProductRequest to select.
     * @return ProductRequest : the ProductRequest object corresponding to the id.
     */
    @Query("SELECT PR FROM ProductRequest PR WHERE PR.id = ?1")
    public ProductRequest getProductRequest(int id);

    /**
     * Delete a specific ProductRequest.
     * @param id : the id of the ProductRequest to delete.
     */
    @Modifying
    @Query("DELETE FROM ProductRequest PR WHERE PR.id = ?1")
    public void deleteProductRequest(int id);

    /**
     * Get the id of a ProductRequest
     */
    @Query("SELECT PR.id FROM ProductRequest PR WHERE PR.userId = ?1 " +
            "AND PR.startDate = ?2 " +
            "AND PR.endDate = ?3 " +
            "AND PR.product = ?4")
    Integer getIdProductRequest(long userId, LocalDate sd, LocalDate ed, Product product);

    @Query("SELECT PR FROM ProductRequest PR WHERE PR.id = ?1")
    ProductRequest existsProductRequest(int id);


}

package com.ProjetCo.location.dto;

import com.ProjetCo.location.model.Article;

public class ProductDTO {
	private int id;
	private double barcode; 
	private String state;
	private boolean isAvailable;
	private boolean isReserved;
	private Article article;
	
	private boolean tp;
	private boolean donation;
	private boolean hs;

	public ProductDTO() {}

	public ProductDTO(double barcode, String state, boolean isAvailable, boolean isReserved, Article article) {
		this.barcode = barcode;
		this.state = state;
		this.isAvailable = isAvailable;
		this.isReserved = isReserved;
		this.article = article;
		tp = false;
		donation = false;
		hs = false;
	}

	public ProductDTO(int id, double barcode, String state, boolean available, boolean reserved, Article article) {
		this.id = id;
		this.barcode = barcode;
		this.state = state;
		this.isAvailable = available;
		this.isReserved = reserved;
		this.article = article;
		tp = false;
		donation = false;
		hs = false;
	}
	
	public ProductDTO(double barcode, String state, boolean isAvailable, boolean isReserved, Article article, boolean tp, boolean donation, boolean hs) {
		this.barcode = barcode;
		this.state = state;
		this.isAvailable = isAvailable;
		this.isReserved = isReserved;
		this.article = article;
		this.tp = tp;
		this.donation = donation;
		this.hs = hs;
	}
	
	public ProductDTO(int id, double barcode, String state, boolean isAvailable, boolean isReserved, Article article, boolean tp, boolean donation, boolean hs) {
		this.id = id;
		this.barcode = barcode;
		this.state = state;
		this.isAvailable = isAvailable;
		this.isReserved = isReserved;
		this.article = article;
		this.tp = tp;
		this.donation = donation;
		this.hs = hs;
	}

	public int getId() { return id; }

	public double getBarcode() {
		return barcode;
	}
	
	public String getState() {
		return state;
	}
	
	public boolean isAvailable() {
		return isAvailable;
	}
	
	public boolean isReserved() {
		return isReserved;
	}
	
	public Article getArticle() {
		return article;
	}
	
	public void setBarcode(double barcode) {
		this.barcode = barcode;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}
	
	public void setReserved(boolean isReserved) {
		this.isReserved = isReserved;
	}
	
	public void setArticle(Article article) {
		this.article = article;
	}

    public void setId(int id) {
    }

	public boolean isTp() {
		return tp;
	}

	public void setTp(boolean tp) {
		this.tp = tp;
	}

	public boolean isDonation() {
		return donation;
	}

	public void setDonation(boolean donation) {
		this.donation = donation;
	}

	public boolean isHs() {
		return hs;
	}

	public void setHs(boolean hs) {
		this.hs = hs;
	}
}
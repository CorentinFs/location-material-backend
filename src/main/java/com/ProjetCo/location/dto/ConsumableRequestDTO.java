package com.ProjetCo.location.dto;

import java.time.LocalDate;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class ConsumableRequestDTO {
	private int id; 
	private String name;
	private long userId;

	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class) 
	private LocalDate startDate;
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class) 
	private LocalDate endDate;
	
	public ConsumableRequestDTO() {
		
	}
	
	public ConsumableRequestDTO(int id, String name, long userId, LocalDate sd, LocalDate ed) {
		this.id = id;
		this.name = name;
		this.userId = userId;
		startDate = sd;
		endDate = ed;
	}
	
	public ConsumableRequestDTO(String name, long userId, LocalDate sd, LocalDate ed) {
		this.name = name;
		this.userId = userId;
		startDate = sd;
		endDate = ed;
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public long getUserId() {
		return userId;
	}
	
	public LocalDate getStartDate() {
		return startDate;
	}
	
	public LocalDate getEndDate() {
		return endDate;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}
}

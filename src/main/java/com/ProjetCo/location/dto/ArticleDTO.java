package com.ProjetCo.location.dto;

import java.util.List;

import com.ProjetCo.location.model.Category;
import com.ProjetCo.location.model.Product;

public class ArticleDTO {	
	private int id;
	private String name;
	private String description;
	private Category category;
	private List<Product> products;
	private byte[] image;
	
	public ArticleDTO() {
		
	}
	
	public ArticleDTO(String name, String description, Category category, List<Product> products, byte[] image) {
		this.name = name;
		this.description = description;
		this.category = category;
		this.products = products;
		this.image = image;
	}
	
	public ArticleDTO(int id, String name, String description, Category category, List<Product> products, byte[] image) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.category = category;
		this.products = products;
		this.image = image;
	}

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public Category getCategory() {
		return category;
	}
	
	public List<Product> getProducts(){
		return products;
	}
	
	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}	
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setCategory(Category category) {
		this.category = category;
	}
	
	public void setProducts(List<Product> products) {
		this.products = products;
	}
}

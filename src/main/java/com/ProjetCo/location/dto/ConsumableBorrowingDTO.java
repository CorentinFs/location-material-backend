package com.ProjetCo.location.dto;

import java.time.LocalDate;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class ConsumableBorrowingDTO {
	private int id; 
	private String name;
	private long userId;
	
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class) 
	private LocalDate startDate;
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate endDate;
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class) 
	private LocalDate renderDate;
	
	public ConsumableBorrowingDTO() {}
	
	public ConsumableBorrowingDTO(int id, String name, long userId, LocalDate sd, LocalDate ed, LocalDate rd) {
		this.id = id;
		this.name = name;
		this.userId = userId;
		startDate = sd;
		endDate = ed;
		renderDate = rd;
	}
	
	public ConsumableBorrowingDTO(String name, long userId, LocalDate sd, LocalDate ed, LocalDate rd) {
		this.name = name;
		this.userId = userId;
		startDate = sd;
		endDate = ed;
		renderDate = rd;
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public long getUserId() {
		return userId;
	}
	
	public LocalDate getStartDate() {
		return startDate;
	}
	
	public LocalDate getEndDate() {
		return endDate;
	}
	
	public LocalDate getRenderDate() {
		return renderDate;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}
	
	public void setRenderDate(LocalDate renderDate) {
		this.renderDate = renderDate;
	}
}


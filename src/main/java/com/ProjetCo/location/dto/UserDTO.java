package com.ProjetCo.location.dto;

public class UserDTO {
	private int id;
	private long userId;
	private String password;
	private boolean isAdmin;
	private boolean isSuperAdmin;
	
	public UserDTO() {}
	
	public UserDTO(int id, long userId, String password, boolean isAdmin, boolean isSuperAdmin) {
		this.id = id;
		this.userId = userId;
		this.password = password;
		this.isAdmin = isAdmin;
		this.isSuperAdmin = isSuperAdmin;
	}
	
	public UserDTO(long userId, String password, boolean isAdmin, boolean isSuperAdmin) {
		this.userId = userId;
		this.password = password;
		this.isAdmin = isAdmin;
		this.isSuperAdmin = isSuperAdmin;
	}
	
	public int getId() {
		return id;
	}
	
	public long getUserId() {
		return userId;
	}
	
	public String getPassword() {
		return password;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public boolean isSuperAdmin() {
		return isSuperAdmin;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public void setSuperAdmin(boolean isSuperAdmin) {
		this.isSuperAdmin = isSuperAdmin;
	}
}

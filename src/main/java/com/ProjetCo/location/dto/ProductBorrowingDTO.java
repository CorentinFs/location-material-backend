package com.ProjetCo.location.dto;


import java.time.LocalDate;

import com.ProjetCo.location.model.Product;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class ProductBorrowingDTO {
	private int id;
	private long userId;
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate startDate;
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate endDate;
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate renderDate;
	private String adminComment;
	private boolean pickedUp;
	private Product product;

	public ProductBorrowingDTO() {}

	public ProductBorrowingDTO(long userId, LocalDate sd, LocalDate ed, LocalDate rd, String adminC, boolean pickedUp, Product product) {
		this.userId = userId;
		startDate = sd;
		endDate = ed;
		renderDate = rd;
		adminComment = adminC;
		this.pickedUp = pickedUp;
		this.product = product;
	}

	public ProductBorrowingDTO(long userId, LocalDate sd, LocalDate ed,String adminC, Product product) {
		this.userId = userId;
		startDate = sd;
		endDate = ed;
		adminComment = adminC;
		this.pickedUp = false;
		this.product = product;
	}

	public ProductBorrowingDTO(int id, long userId, LocalDate sd, LocalDate ed, LocalDate rd, String adminC, boolean pickedUp, Product product) {
		this.id = id;
		this.userId = userId;
		startDate = sd;
		endDate = ed;
		renderDate = rd;
		adminComment = adminC;
		this.pickedUp = pickedUp;
		this.product = product;
	}

	public int getId() {
		return id;
	}
	
	public long getUserId() {
		return userId;
	}
	
	public LocalDate getStartDate() {
		return startDate;
	}
	
	public LocalDate getEndDate() {
		return endDate;
	}
	
	public LocalDate getRenderDate() {
		return renderDate;
	}
	
	public String getAdminComment() {
		return adminComment;
	}
	
	public boolean getPickedUp() {
		return pickedUp;
	}
	
	public Product getProduct() {
		return product;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public void setRenderDate(LocalDate renderDate) {
		this.renderDate = renderDate;
	}

	public void setAdminComment(String adminComment) {
		this.adminComment = adminComment;
	}

	public void setPickedUp(boolean pickedUp) {
		this.pickedUp = pickedUp;
	}

	public void setProduct(Product product) {
		this.product = product;
	}	
}

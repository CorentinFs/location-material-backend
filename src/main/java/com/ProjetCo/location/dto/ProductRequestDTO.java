package com.ProjetCo.location.dto;

import java.time.LocalDate;

import com.ProjetCo.location.model.Product;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class ProductRequestDTO {
	private int id;
	private long userId;
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate startDate;
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate endDate;
	private Product product;

	public ProductRequestDTO() {}

	public ProductRequestDTO(long userId, LocalDate sd, LocalDate ed, Product product) {
		this.userId = userId;
		this.startDate = sd;
		this.endDate = ed;
		this.product = product;
	}

	public ProductRequestDTO(int id, long userId, LocalDate sd, LocalDate ed, Product product) {
		this.id = id;
		this.userId = userId;
		this.startDate = sd;
		this.endDate = ed;
		this.product = product;
	}

	public int getId() {
		return id;
	}
	
	public long getUserId() {
		return userId;
	}
	
	public LocalDate getStartDate() {
		return startDate;
	}
	
	public LocalDate getEndDate() {
		return endDate;
	}
	
	public Product getProduct() {
		return product;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}
	
	public void setProduct(Product product) {
		this.product = product;
	}	
}

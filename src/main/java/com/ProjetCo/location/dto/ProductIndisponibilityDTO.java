package com.ProjetCo.location.dto;

import java.util.List;

public class ProductIndisponibilityDTO {
	private ProductDTO productDTO;
	private List<List<String>> indisponibilityDates;
	
	public ProductIndisponibilityDTO() {}
	
	public ProductIndisponibilityDTO(ProductDTO product, List<List<String>> dates) {
		productDTO = product;
		indisponibilityDates = dates;
	}

	public ProductDTO getProductDTO() {
		return productDTO;
	}

	public void setProductDTO(ProductDTO productDTO) {
		this.productDTO = productDTO;
	}

	public List<List<String>> getIndisponibilityDates() {
		return indisponibilityDates;
	}

	public void setIndisponibilityDates(List<List<String>> indisponibilityDates) {
		this.indisponibilityDates = indisponibilityDates;
	}
}

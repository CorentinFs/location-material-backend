package com.ProjetCo.location.dto;

import java.util.ArrayList;
import java.util.List;

import com.ProjetCo.location.model.Article;

public class CategoryDTO {
	private int id;
	private String name;
	private List<Article> articles;
	
	public CategoryDTO() {
		this.articles = new ArrayList<Article>();
	}
	
	public CategoryDTO(String name, List<Article> articles) {
		this.name = name;
		this.articles = articles;
	}
	
	public CategoryDTO(int id, String name, List<Article> articles) {
		this.id = id;
		this.name = name;
		this.articles = articles;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	public String getName() {
		return name;
	}
	
	public List<Article> getArticles(){
		return articles;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}

package com.ProjetCo.location.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ProjetCo.location.dto.CategoryDTO;
import com.ProjetCo.location.model.Category;
import com.ProjetCo.location.service.CategoryServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rest/category/api")
@Api(value = "Category Rest Controller: contains all operations for managing category")
public class CategoryController {
	public static final Logger LOGGER = LoggerFactory.getLogger(CategoryController.class);
	
	@Autowired
	private CategoryServiceImpl categoryServiceImpl;
	
	/**
     * Get a Category given a name.
     * @param name : the name of the Category to search in the database.
     * @return
     */
    @GetMapping("/getCategoryByName")
    @ApiOperation(value = "Get the Category corresponding to the given name.", response = CategoryDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 409, message = "Conflict : the name does not correspond to an Category."),
    						@ApiResponse(code = 200, message = "The Category is correctly returned.") })
    public ResponseEntity<CategoryDTO> getArticleByName(@RequestParam(name = "name") String name)
    {
    	boolean existsName = categoryServiceImpl.existsCategory(name);
    	if(!existsName) {
    		return new ResponseEntity<CategoryDTO>(HttpStatus.CONFLICT);
    	}
    	
    	Category category = categoryServiceImpl.getCategory(name);
    	CategoryDTO categoryDTO = categoryToCategoryDTO(category);
    	
    	return new ResponseEntity<CategoryDTO>(categoryDTO, HttpStatus.OK);
    }
	
	/**
	 * Insert a Category into the DB.
	 * If the Category already exists, return HTTP_CONFLICT
	 * If the Category doesn't exists and is properly added, return HTTP_CREATED
	 * @param categoryDTO : the DTO of the Entity Category
	 * @return
	 */
	@PostMapping("/createCategory")
	@ApiOperation(value = "Add a new Category for Products", response = CategoryDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 409, message = "Conflict: the category already exist"),
            				@ApiResponse(code = 201, message = "Created: the category is successfully inserted"),
            				@ApiResponse(code = 304, message = "Not Modified: the category is unsuccessfully inserted") })
	public ResponseEntity<CategoryDTO> createCategory(@RequestBody CategoryDTO categoryDTO) {
		boolean existsCategory = categoryServiceImpl.existsCategory(categoryDTO.getName());
		if (existsCategory) {
			return new ResponseEntity<CategoryDTO>(HttpStatus.CONFLICT);
		}
		
		//Save the new Category into the DB
		Category categoryRequest = categoryDTOToCategory(categoryDTO);
		Category category = categoryServiceImpl.saveCategory(categoryRequest);
		
		CategoryDTO resCategoryDTO = categoryToCategoryDTO(category);

		if(category != null) {
			return new ResponseEntity<CategoryDTO>(resCategoryDTO, HttpStatus.CREATED);
		}
		
		return new ResponseEntity<CategoryDTO>(HttpStatus.NOT_MODIFIED);
	}
	
	/**
	 * Delete a Category from the DB.
	 * If the Category exists and is properly deleted, return HTTP_NO_CONTENT
	 * @param categoryId : id of the Category to delete
	 * @return
	 */
	@RequestMapping(value="/deleteCategory/{categoryId}", method = {RequestMethod.DELETE})
	@ApiOperation(value = "Delete a given Category", response = CategoryDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 409, message = "The category to delete doesn't exist."),
    						@ApiResponse(code = 204, message = "The category is properly deleted.") })
	public ResponseEntity<String> deleteCategory(@PathVariable Integer categoryId) {
		//Check if the Category's id exists
		boolean existsCategory = categoryServiceImpl.existsCategory(categoryId);
		if (!existsCategory) {
			return new ResponseEntity<String>(HttpStatus.CONFLICT);
		}
		
		//delete the Category from the DB
		categoryServiceImpl.deleteCategory(categoryId);
		return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
	}
	
	/**
	 * Get the List of all CategoryDTO registered.
	 * @return
	 */
	@GetMapping("/getAllCategories")
	@ApiOperation(value = "Get the List of all CategoryDTO.", response = CategoryDTO.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The List of CategoryDTO is correctly retrieved.") })
	public ResponseEntity<List<CategoryDTO>> getAllCategory(){
		List<CategoryDTO> listCategoryDTO = listCategoryToListCategoryDTOs(categoryServiceImpl.findAll());
		return new ResponseEntity<List<CategoryDTO>>(listCategoryDTO, HttpStatus.OK);
	}
	
	/**
	 * Get the Category object corresponding to an ID.
	 * @return
	 */
	@GetMapping("/getCategoryById")
	@ApiOperation(value = "Get the Category corresponding to an ID", response = CategoryDTO.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The CategoryDTO is correctly retrieved."),
							@ApiResponse(code = 409, message = "The CategoryDTO doesn't exist.")})
	public ResponseEntity<CategoryDTO> getCategoryById(@RequestParam(value = "id") int categoryId) {
		Optional<Category> category = categoryServiceImpl.findById(categoryId);
		
		if(category.isPresent()) {
			CategoryDTO categoryDTO = categoryToCategoryDTO(category.get());
			return new ResponseEntity<CategoryDTO>(categoryDTO, HttpStatus.OK);
		}
		
		return new ResponseEntity<CategoryDTO>(HttpStatus.CONFLICT);
	}
	
	/**
	 * Convert a List of Category into a List of CategoryDTO
	 * @param categoryList
	 * @return
	 */
	public List<CategoryDTO> listCategoryToListCategoryDTOs(List<Category> categoryList){
		ArrayList<CategoryDTO> categoryDTOs = new ArrayList<CategoryDTO>();
		
		for(Category cat : categoryList) {
			CategoryDTO temp = new CategoryDTO(cat.getId(), cat.getName(), cat.getArticles());
			categoryDTOs.add(temp);
		}
		
		return categoryDTOs;
	}
	
	/**
	 * Convert a Category into a CategoryDTO
	 * @param category
	 * @return
	 */
	public CategoryDTO categoryToCategoryDTO(Category category){
		CategoryDTO categoryDTO = new CategoryDTO(category.getId(), category.getName(), category.getArticles());
		
		return categoryDTO;
	}
	
	/**
	 * Transform a CategoryDTO into a Category entity object.
	 * @param categoryDTO : the categoryDTO To transform.
	 * @return Category : the Category entity corresponding.
	 */
	public Category categoryDTOToCategory(CategoryDTO categoryDTO) {
		return new Category(categoryDTO.getName(), categoryDTO.getArticles());
	}
}

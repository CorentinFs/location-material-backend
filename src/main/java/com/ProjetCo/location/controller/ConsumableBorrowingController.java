package com.ProjetCo.location.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.ProjetCo.location.dto.ConsumableBorrowingDTO;
import com.ProjetCo.location.model.ConsumableBorrowing;
import com.ProjetCo.location.service.ConsumableBorrowingServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rest/consumableborrowing/api")
@Api(value = "ConsumableBorrowing Rest Controller: contains all operations for managing consumable borrowing")
public class ConsumableBorrowingController {
	public static final Logger LOGGER = LoggerFactory.getLogger(ConsumableBorrowingController.class);
	
	@Autowired
	private ConsumableBorrowingServiceImpl consumableServiceImpl;
	
	/**
     * Update a ConsumableBorrowing
     * @param ConsumableBorrowingDTO corresponding to the ConsumableBorrowing to update
     * @return
     */
    @PutMapping("/updateConsumableBorrowing")
    @ApiOperation(value = "Update a ConsumableBorrowing.", response = ConsumableBorrowingDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 409, message = "The ConsumableBorrowing doesn't exist."),
    						@ApiResponse(code = 201, message = "The ConsumableBorrowing is correctly updated."),
    						@ApiResponse(code = 304, message = "The ConsumableBorrowing is unsuccessfully updated")})
    public ResponseEntity<ConsumableBorrowingDTO> updateConsumableBorrowing(@RequestBody ConsumableBorrowingDTO consumableBorrowingDTO) {
        //Check if the consumableBorrowing exists
    	if (!consumableServiceImpl.existsConsumableBorrowing(consumableBorrowingDTO.getId())) {
            return new ResponseEntity<ConsumableBorrowingDTO>(HttpStatus.CONFLICT);
        }
        ConsumableBorrowing consumableBorrowingRequest = consumableServiceImpl.getConsumableBorrowing(consumableBorrowingDTO.getId());
        
        //Update
        consumableBorrowingRequest.setEndDate(consumableBorrowingDTO.getEndDate());
        consumableBorrowingRequest.setName(consumableBorrowingDTO.getName());
        consumableBorrowingRequest.setRenderDate(consumableBorrowingDTO.getRenderDate());
        consumableBorrowingRequest.setStartDate(consumableBorrowingDTO.getStartDate());
        consumableBorrowingRequest.setUserId(consumableBorrowingDTO.getUserId());
        
        ConsumableBorrowing consumableResponse = consumableServiceImpl.saveConsumableBorrowing(consumableBorrowingRequest);
        
        if (consumableResponse != null) {
            return new ResponseEntity<ConsumableBorrowingDTO>(consumableBorrowingDTO, HttpStatus.CREATED);
        }
        return new ResponseEntity<ConsumableBorrowingDTO>(HttpStatus.NOT_MODIFIED);
    }
    
	/**
	 * Get the List of all returned ConsumableBorrowing related to a User.
	 * @param userId : the userId of the User.
	 * @return
	 */
	@GetMapping("/getReturnedConsumableBorrowingByUserId")
	@ApiOperation(value = "Get the List of all returned ConsumableBorrowing made by a User.", response = ConsumableBorrowingDTO.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The List of returned ConsumableBorrowing is correctly retrieved.")})
	public ResponseEntity<List<ConsumableBorrowingDTO>> getReturnedConsumableBorrowingByUserId(@RequestParam(value = "userId") String userId){
		List<ConsumableBorrowing>  allConsumableBorrowing = consumableServiceImpl.getReturnedConsumableBorrowingByUserId(Long.parseLong(userId));
		List<ConsumableBorrowingDTO> allConsumableBorrowingDTO = listConsumableBorrowingToListConsumableBorrowingDTO(allConsumableBorrowing);

		return new ResponseEntity<List<ConsumableBorrowingDTO>>(allConsumableBorrowingDTO,HttpStatus.OK);
	}

	/**
	 * Get the List of all ConsumableBorrowing related to a User.
	 * @param userId : the userId of the User.
	 * @return
	 */
	@GetMapping("/getConsumableBorrowingByUserId")
	@ApiOperation(value = "Get the List of all ConsumableBorrowing made by a User.", response = ConsumableBorrowingDTO.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The List of ConsumableBorrowing is correctly retrieved.")})
	public ResponseEntity<List<ConsumableBorrowingDTO>> getConsumableBorrowingByUserId(@RequestParam(value = "userId") String userId){
		List<ConsumableBorrowing>  allConsumableBorrowing = consumableServiceImpl.getConsumableBorrowingByUserId(Long.parseLong(userId));
		List<ConsumableBorrowingDTO> allConsumableBorrowingDTO = listConsumableBorrowingToListConsumableBorrowingDTO(allConsumableBorrowing);

		return new ResponseEntity<List<ConsumableBorrowingDTO>>(allConsumableBorrowingDTO,HttpStatus.OK);
	}
	/**
	 * Insert a ConsumableBorrowing into the DB.
	 * If the ConsumableBorrowing already exists, return HTTP_CONFLICT
	 * If the ConsumableBorrowing doesn't exists and is properly added, return HTTP_CREATED
	 * @param consumableBorrowingDTO : the DTO of the Entity ConsumableBorrowing
	 * @return
	 */
	@SuppressWarnings("unused")
	@PostMapping("/createConsumableBorrowing")
	@ApiOperation(value = "Add a new ConsumableBorrowing", response = ConsumableBorrowingDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Created: the ConsumableBorrowing is successfully inserted"),
            				@ApiResponse(code = 304, message = "Not Modified: the ConsumableBorrowing is unsuccessfully inserted") })
	public ResponseEntity<ConsumableBorrowingDTO> createConsumableBorrowing(@RequestBody ConsumableBorrowingDTO consumableBorrowingDTO) {
		//Save the new ConsumableBorrowing into the DB
		ConsumableBorrowing tempConsumableBorrowing = new ConsumableBorrowing(consumableBorrowingDTO.getName(), consumableBorrowingDTO.getUserId(),
				consumableBorrowingDTO.getStartDate(), consumableBorrowingDTO.getEndDate(), consumableBorrowingDTO.getRenderDate());
		ConsumableBorrowing consumableBorrowing = consumableServiceImpl.saveConsumableBorrowing(tempConsumableBorrowing);
		
		//save of the id in the dto
		consumableBorrowingDTO.setId(consumableBorrowing.getId());

		if(consumableBorrowing != null) {
			return new ResponseEntity<ConsumableBorrowingDTO>(consumableBorrowingDTO, HttpStatus.CREATED);
		}

		return new ResponseEntity<ConsumableBorrowingDTO>(HttpStatus.NOT_MODIFIED);
	}
	
	/**
	 * Delete a ConsumableBorrowing from the DB.
	 * If the ConsumableBorrowing doesn't exists, return HTTP_CONFLICT
	 * If the ConsumableBorrowing exists and is properly deleted, return HTTP_NO_CONTENT
	 * @param consumableBorrowingId : id of the ConsumableBorrowing to delete
	 * @return
	 */
	@RequestMapping(value="/deleteConsumableBorrowing/{consumableBorrowingId}", method = {RequestMethod.DELETE})
	@ApiOperation(value = "Delete a given ConsumableBorrowing", response = ConsumableBorrowingDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 204, message = "The ConsumableBorrowing is properly deleted."),
    						@ApiResponse(code = 409, message = "The ConsumableBorrowing doesn't exist")})
	public ResponseEntity<Boolean> deleteConsumableBorrowing(@PathVariable Integer consumableBorrowingId) {
		//check if the consumableBorrowing exists
		boolean existsCB = consumableServiceImpl.existsConsumableBorrowing(consumableBorrowingId);
		if (!existsCB) {
			return new ResponseEntity<Boolean>(false, HttpStatus.CONFLICT);
		}
		
		//delete the ConsumableRequest from the DB
		consumableServiceImpl.deleteConsumableBorrowing(consumableBorrowingId);
		return new ResponseEntity<Boolean>(true, HttpStatus.NO_CONTENT);
	}
	
	/**
	 * Get the list of all ConsumableBorrowingDTO registered
	 * @return
	 */
	@GetMapping("/getAllConsumableBorrowings")
	@ApiOperation(value = "Get the List of all ConsumableBorrowingDTO.", response = ConsumableBorrowingDTO.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The List of ConsumableBorrowingDTO is correctly retrived.") })
	public ResponseEntity<List<ConsumableBorrowingDTO>> getAllConsumableBorrowing() {
		List<ConsumableBorrowing>  allConsumableBorrowings = consumableServiceImpl.findAll();
		List<ConsumableBorrowingDTO> allConsumableBorrowingDTO = listConsumableBorrowingToListConsumableBorrowingDTO(allConsumableBorrowings);
	
		return new ResponseEntity<List<ConsumableBorrowingDTO>>(allConsumableBorrowingDTO, HttpStatus.OK);
	}
	
	/**
	 * Convert a list of ConsumableBorrowing into a list of ConsumableBorrowingDTO
	 * @param consumableBorrowingList
	 * @return
	 */
	public List<ConsumableBorrowingDTO> listConsumableBorrowingToListConsumableBorrowingDTO(List<ConsumableBorrowing> consumableBorrowingList) {
		ArrayList<ConsumableBorrowingDTO> consumableBorrowingDTOs = new ArrayList<ConsumableBorrowingDTO>();
		
		for (ConsumableBorrowing cb : consumableBorrowingList) {
			ConsumableBorrowingDTO temp = new ConsumableBorrowingDTO(cb.getId(), cb.getName(), cb.getUserId(), cb.getStartDate(), cb.getEndDate(),
					cb.getRenderDate());
			consumableBorrowingDTOs.add(temp);
		}
		
		return consumableBorrowingDTOs;
	}
	
	/**
     * Convert a ConsumableBorrowing into a ConsumableBorrowingDTO
     * @param consumableBorrowing : the object to convert
     * @return
     */
    public ConsumableBorrowingDTO consumableBorrowingToConsumableBorrowingDTO(ConsumableBorrowing consumableBorrowing) {
        return new ConsumableBorrowingDTO(consumableBorrowing.getId(), 
        		consumableBorrowing.getName(), 
        		consumableBorrowing.getUserId(), 
        		consumableBorrowing.getStartDate(), 
				consumableBorrowing.getEndDate(), 
				consumableBorrowing.getRenderDate());
    }
}

package com.ProjetCo.location.controller;

import java.util.ArrayList;
import java.util.List;

import com.ProjetCo.location.model.ConsumableBorrowing;
import com.ProjetCo.location.service.ConsumableBorrowingServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ProjetCo.location.dto.ConsumableRequestDTO;
import com.ProjetCo.location.model.ConsumableRequest;
import com.ProjetCo.location.service.ConsumableRequestServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rest/consumablerequest/api")
@Api(value = "ConsumableRequest Rest Controller: contains all operations for managing consumable request")
public class ConsumableRequestController {
	public static final Logger LOGGER = LoggerFactory.getLogger(ConsumableRequestController.class);
	
	@Autowired
	private ConsumableRequestServiceImpl consumableServiceImpl;
	@Autowired
	private ConsumableBorrowingServiceImpl consumableBorrowingService;
	
	/**
     * Get the List of all ConsumableRequests related to a User.
     * @param userId : the userId of the User.
     * @return
     */
    @GetMapping("/getConsumableRequestByUserId")
    @ApiOperation(value = "Get the List of all ConsumableRequest made by a User.", response = ConsumableRequestDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "The List of ConsumableRequest is correctly retrived.")})
    public ResponseEntity<List<ConsumableRequestDTO>> getConsumableRequestByUserId(@RequestParam(value = "userId") String userId){
    	List<ConsumableRequest>  allConsumableRequests = consumableServiceImpl.getConsumableRequestsByUserId(Long.parseLong(userId));
        List<ConsumableRequestDTO> allConsumableRequestDTO = listConsumableRequestToListConsumableRequestDTO(allConsumableRequests);

        return new ResponseEntity<List<ConsumableRequestDTO>>(allConsumableRequestDTO, HttpStatus.OK);
    }
	
	/**
	 * Insert a ConsumableRequest into the DB.
	 * If the ConsumableRequest already exists, return HTTP_CONFLICT
	 * If the ConsumableRequest doesn't exists and is properly added, return HTTP_CREATED
	 * @param consumableRequestDTO : the DTO of the Entity ConsumableRequest
	 * @return
	 */
	@PostMapping("/createConsumableRequest")
	@ApiOperation(value = "Add a new ConsumableRequest", response = ConsumableRequestDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Created: the ConsumableRequest is successfully inserted"),
            				@ApiResponse(code = 304, message = "Not Modified: the ConsumableRequest is unsuccessfully inserted") })
	public ResponseEntity<ConsumableRequestDTO> createConsumableRequest(@RequestBody ConsumableRequestDTO consumableRequestDTO) {		
		//Save the new ConsumableRequest into the DB
		ConsumableRequest tempConsumableRequest = new ConsumableRequest(consumableRequestDTO.getName(), consumableRequestDTO.getUserId(), consumableRequestDTO.getStartDate(), 
				consumableRequestDTO.getEndDate());
		ConsumableRequest consumableRequest = consumableServiceImpl.saveConsumableRequest(tempConsumableRequest);
		ConsumableRequestDTO res = consumableRequestToConsumableRequestDTO(consumableRequest);

		if(consumableRequest != null) {
			return new ResponseEntity<ConsumableRequestDTO>(res, HttpStatus.CREATED);
		}
		
		return new ResponseEntity<ConsumableRequestDTO>(HttpStatus.NOT_MODIFIED);
	}
	
	/**
	 * Validate a request of consumable made by a user.
	 * @param consumableRequestId : the id of the consumable request to validate.
	 * @return
	 */
	@RequestMapping(value="/validateConsumableRequest/{consumableRequestId}", method = {RequestMethod.POST})
	@ApiOperation(value = "Delete a given ConsumableRequest", response = ConsumableRequestDTO.class)
	@ApiResponses(value = { @ApiResponse(code = 204, message = "The ConsumableRequest is properly validated."),
							@ApiResponse(code = 409, message = "The ProductRequest doesn't exist")})
	public ResponseEntity<Boolean> validateProductRequest(@PathVariable int consumableRequestId) {
		boolean existsConsumableRequest = consumableServiceImpl.existsConsumableRequest(consumableRequestId);
		if (!existsConsumableRequest) {
			return new ResponseEntity<Boolean>(false, HttpStatus.CONFLICT);
		}
		
		//If the ConsumableRequest exists then we can create a ConsumableBorrowing with the given parameters in the request
		ConsumableRequest cr = consumableServiceImpl.getConsumableRequest(consumableRequestId);
		ConsumableBorrowing cb = new ConsumableBorrowing(cr.getName(), cr.getUserId(), cr.getStartDate(), cr.getEndDate());
		consumableBorrowingService.saveConsumableBorrowing(cb);
		
		//Delete the request because we don't need it anymore
		consumableServiceImpl.deleteConsumableRequest(consumableRequestId);
		
		return new ResponseEntity<Boolean>(true, HttpStatus.NO_CONTENT);
	}

	/**
	 * Delete a ConsumableRequest from the DB.
	 * @param consumableRequestId : id of the ConsumableRequest to delete
	 * @return
	 */
	@RequestMapping(value="/deleteConsumableRequest/{consumableRequestId}", method = {RequestMethod.DELETE, RequestMethod.POST})
	@ApiOperation(value = "Delete a given ConsumableRequest", response = Boolean.class)
    @ApiResponses(value = { @ApiResponse(code = 204, message = "The ConsumableRequest is properly deleted."),
    						@ApiResponse(code = 409, message = "The ConsumableRequest doesn't exist")})
	public ResponseEntity<Boolean> deleteConsumableRequest(@PathVariable Integer consumableRequestId) {
		//check if the consumableRequest exists
		boolean existsConsumableRequest = consumableServiceImpl.existsConsumableRequest(consumableRequestId);
		if (!existsConsumableRequest) {
			return new ResponseEntity<Boolean>(false, HttpStatus.CONFLICT);
		}
		
		//delete the ConsumableRequest from the DB
		consumableServiceImpl.deleteConsumableRequest(consumableRequestId);
		return new ResponseEntity<Boolean>(true, HttpStatus.NO_CONTENT);
	}
	
	/**
	 * Get the list of all ConsumableRequestDTO registered
	 * @return
	 */
	@GetMapping("/getAllConsumableRequests")
	@ApiOperation(value = "Get the List of all ConsumableRequestDTO.", response = ConsumableRequestDTO.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The List of ConsumableRequestDTO is correctly retrived.") })
	public ResponseEntity<List<ConsumableRequestDTO>> getAllConsumableRequest() {
		List<ConsumableRequest>  allConsumableRequests = consumableServiceImpl.findAll();
		List<ConsumableRequestDTO> allConsumableRequestDTO = listConsumableRequestToListConsumableRequestDTO(allConsumableRequests);
	
		return new ResponseEntity<List<ConsumableRequestDTO>>(allConsumableRequestDTO, HttpStatus.OK);
	}
	
	/**
	 * Convert a list of ConsumableRequest into a list of ConsumableRequestDTO
	 * @param ConsumableRequestList
	 * @return
	 */
	public List<ConsumableRequestDTO> listConsumableRequestToListConsumableRequestDTO(List<ConsumableRequest> consumableRequestList) {
		ArrayList<ConsumableRequestDTO> consumableRequestDTOs = new ArrayList<ConsumableRequestDTO>();
		
		for (ConsumableRequest cr : consumableRequestList) {
			ConsumableRequestDTO temp = new ConsumableRequestDTO(cr.getId(), cr.getName(), cr.getUserId(), cr.getStartDate(), cr.getEndDate());
			consumableRequestDTOs.add(temp);
		}
		
		return consumableRequestDTOs;
	}
	
	/**
	 * Transform a ConsumableRequest into a ConsumableRequestDTO entity object
	 * @param ConsumableRequest : the ConsumableRequest to transform
	 * @return ConsumableRequestDTO : the ConsumableRequestDTO entity corresponding
	 */
	public ConsumableRequestDTO consumableRequestToConsumableRequestDTO(ConsumableRequest consumableRequest) {
		return new ConsumableRequestDTO(consumableRequest.getId(), 
				consumableRequest.getName(), 
				consumableRequest.getUserId(), 
				consumableRequest.getStartDate(), 
				consumableRequest.getEndDate());
	}
}

package com.ProjetCo.location.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ProjetCo.location.dto.UserDTO;
import com.ProjetCo.location.model.User;
import com.ProjetCo.location.service.UserServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rest/connexion/api")
@Api(value = "Connexion Rest Controller: contains all operations for managing connexion")
public class ConnexionController {
	public static final Logger LOGGER = LoggerFactory.getLogger(ConnexionController.class);
	
	@Autowired
	private UserServiceImpl userServiceImpl;
	
	/**
	 * Verify if the person who wants to connect exists
	 * @return
	 */
	@GetMapping("/connect")
	@ApiOperation(value = "Verify if the person who wants to connect exists, return the UserDTO if exists", response = UserDTO.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The UserDTO is correctly retrived."),
							@ApiResponse(code = 409, message = "The logins don't correspond to a user")})
	public ResponseEntity<UserDTO> connect(@RequestParam(name = "userId") long userId, @RequestParam(name = "password") String password) {
		//Check for Users
		boolean existsUser = userServiceImpl.connect(userId, password);
		if (existsUser) {
			User user = userServiceImpl.getUser(userId);
			UserDTO userDTO = userToUserDTO(user);
			
			return new ResponseEntity<UserDTO>(userDTO, HttpStatus.OK);
		}
		
		return new ResponseEntity<UserDTO>(HttpStatus.CONFLICT);
	}
	
	/**
	 * Transform a User into a UserDTO
	 * @param user : the User to transform
	 * @param type : the type of the UserDTO
	 * @return UserDTO : the UserDTO object corresponding to the User entity object
	 */
	public UserDTO userToUserDTO(User user) {
		return new UserDTO(user.getId(), user.getUserId(), user.getPassword(), user.isAdmin(), user.isSuperAdmin());
	}
}

package com.ProjetCo.location.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ProjetCo.location.dto.UserDTO;
import com.ProjetCo.location.model.User;
import com.ProjetCo.location.service.UserServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rest/user/api")
@Api(value = "User Rest Controller: contains all operations for managing user")
public class UserController {
	public static final Logger LOGGER = LoggerFactory.getLogger(ArticleController.class);
	
	@Autowired
    private UserServiceImpl userServiceImpl;
	
	/**
	 * Update a user to administrator
	 * @param userDTO : the UserDTO corresponding to the User to update
	 * @return
	 */
	@PostMapping("/createAdministrator")
	@ApiOperation(value = "Update a User to an Administrator", response = Boolean.class)
    @ApiResponses(value = { @ApiResponse(code = 409, message = "The user doesn't exist"),
            				@ApiResponse(code = 201, message = "The user has been successfully update to administrator"),
            				@ApiResponse(code = 304, message = "The user has been unsuccessfully update to administrator") })
	public ResponseEntity<UserDTO> createAdministrator(@RequestBody UserDTO userDTO) {
		//Check if the User exists
        boolean existsUser = userServiceImpl.existsUser(userDTO.getUserId());
        if (!existsUser) {
            return new ResponseEntity<UserDTO>(HttpStatus.CONFLICT);
        }

        //Save the User into the DB
        User user = userServiceImpl.getUser(userDTO.getUserId());
        user.setAdmin(true);
        User temp = userServiceImpl.saveUser(user);
        UserDTO resDTO = userToUserDTO(temp);
        
        if (temp != null) {
        	return new ResponseEntity<UserDTO>(resDTO, HttpStatus.CREATED);
        }
        
        return new ResponseEntity<UserDTO>(HttpStatus.NOT_MODIFIED);
    }
	
	/**
	 * Update an administrator to user
	 * @param userDTO : the UserDTO corresponding to the User to update
	 * @return
	 */
	@PostMapping("/deleteAdministrator")
	@ApiOperation(value = "Update an Administrator to a User", response = Boolean.class)
    @ApiResponses(value = { @ApiResponse(code = 409, message = "The user doesn't exist"),
            				@ApiResponse(code = 201, message = "The administrator has been successfully update to user"),
            				@ApiResponse(code = 304, message = "The administrator has been unsuccessfully update to user") })
	public ResponseEntity<UserDTO> deleteAdministrator(@RequestBody UserDTO userDTO) {
		//Check if the User exists
        boolean existsUser = userServiceImpl.existsUser(userDTO.getUserId());
        if (!existsUser) {
            return new ResponseEntity<UserDTO>(HttpStatus.CONFLICT);
        }

        //Save the User into the DB
        User user = userServiceImpl.getUser(userDTO.getUserId());
        user.setAdmin(false);
        User temp = userServiceImpl.saveUser(user);
        UserDTO resDTO = userToUserDTO(temp);
        
        if (temp != null) {
        	return new ResponseEntity<UserDTO>(resDTO, HttpStatus.CREATED);
        }
        
        return new ResponseEntity<UserDTO>(HttpStatus.NOT_MODIFIED);
    }
	
	/**
     * Get a user by his userId
     * @param userId : the userId of the User (be carreful, userId is different than id).
     * @return
     */
    @GetMapping("/getUserByUserId")
    @ApiOperation(value = "Get the User corresponding to a userId.", response = UserDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "The User is correctly retrived."),
    						@ApiResponse(code = 409, message = "The User doesn't exist")})
    public ResponseEntity<UserDTO> getUserByUserId(@RequestParam(value = "userId") long userId){
    	User user = userServiceImpl.getUser(userId);
    	
    	if(user != null) {
    		UserDTO userDTO = userToUserDTO(user);
    		return new ResponseEntity<UserDTO>(userDTO, HttpStatus.CREATED);
    	}
    	
    	return new ResponseEntity<UserDTO>(HttpStatus.CONFLICT);
    }
	
	/**
	 * Transform a User into a UserDTO
	 * @param user : object to transform
	 */
	public UserDTO userToUserDTO(User user)
	{
		return new UserDTO(user.getId(), user.getUserId(), user.getPassword(), user.isAdmin(), user.isSuperAdmin());
	}
}

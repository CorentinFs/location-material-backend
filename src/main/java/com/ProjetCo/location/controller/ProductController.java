package com.ProjetCo.location.controller;

import com.ProjetCo.location.dto.ProductDTO;
import com.ProjetCo.location.model.Product;
import com.ProjetCo.location.model.ProductBorrowing;
import com.ProjetCo.location.model.ProductRequest;
import com.ProjetCo.location.service.ProductBorrowingServiceImpl;
import com.ProjetCo.location.service.ProductRequestServiceImpl;
import com.ProjetCo.location.service.ProductServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/rest/product/api")
@Api(value = "Product Rest Controller: contains all operations for managing product")
public class ProductController {
    public static final Logger LOGGER = LoggerFactory.getLogger(ProductDTO.class);

    @Autowired
    private ProductServiceImpl productService;
    @Autowired
    private ProductRequestServiceImpl productRequestService;
    @Autowired
    private ProductBorrowingServiceImpl productBorrowingService;
    /**
     * Insert a Product into the DB.
     * @return
     */
    @SuppressWarnings("unused")
	@PostMapping("/createProduct")
    @ApiOperation(value = "Add a new Product into the database.", response = ProductDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 409, message = "The product already exist"),
            				@ApiResponse(code = 201, message = "The product is successfully inserted"),
            				@ApiResponse(code = 304, message = "The product is unsuccessfully inserted")})
    public ResponseEntity<ProductDTO> createProduct(@RequestBody ProductDTO productDTO) {
        //Check if the product exists
    	boolean existsProduct = productService.existsProduct(productDTO.getBarcode());
        if (existsProduct) {
            return new ResponseEntity<ProductDTO>(HttpStatus.CONFLICT);
        }

        //Save the new Product into the DB
        Product productRequest = productDTOtoProduct(productDTO);
        Product product = productService.saveProduct(productRequest);
        productDTO.setBarcode(product.getBarcode());
        productDTO.setId(product.getId());
        
        if (product != null) {
            return new ResponseEntity<ProductDTO>(productDTO, HttpStatus.CREATED);
        }

        return new ResponseEntity<ProductDTO>(HttpStatus.NOT_MODIFIED);
    }
    
    /**
     * Update a Product from the database
     * @param productDTO corresponding to the Product to update
     * @return
     */
    @PutMapping("/updateProduct")
    @ApiOperation(value = "Update a Product from the database.", response = ProductDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 409, message = "The product doesn't exist"),
            				@ApiResponse(code = 201, message = "The product is successfully updated"),
            				@ApiResponse(code = 304, message = "The product is unsuccessfully updated")})
    public ResponseEntity<ProductDTO> updateProduct(@RequestBody ProductDTO productDTO) {
    	//Check if the Product exists
    	if (!productService.existsProduct(productDTO.getBarcode())) {
            return new ResponseEntity<ProductDTO>(HttpStatus.CONFLICT);
        }
    	
        Product productRequest = productService.getProduct(productDTO.getBarcode());
        
        //Update
        productRequest.setAvailable(productDTO.isAvailable());
        productRequest.setReserved(productDTO.isReserved());
        productRequest.setState(productDTO.getState());
        productRequest.setArticle(productDTO.getArticle());
        productRequest.setHs(productDTO.isHs());
        productRequest.setTp(productDTO.isTp());
        productRequest.setDonation(productDTO.isDonation());
        
        Product productResponse = productService.saveProduct(productRequest);
        
        if (productResponse != null) {
        	ProductDTO productDTOToReturn = productToProductDTO(productResponse);
            return new ResponseEntity<ProductDTO>(productDTOToReturn, HttpStatus.CREATED);
        }
        return new ResponseEntity<ProductDTO>(HttpStatus.NOT_MODIFIED);
    }

    /**
     * Delete a Product from the DB.
     * @param productId : id of the Product to delete
     * @return
     */
    @RequestMapping(value="/deleteProduct/{productId}", method = {RequestMethod.DELETE, RequestMethod.POST})
    @ApiOperation(value = "Delete a given Product", response = ProductDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 204, message = "The Product is properly deleted."),
    						@ApiResponse(code = 409, message = "The Product doesn't exist")})
    public ResponseEntity<Boolean> deleteProduct(@PathVariable double productId) {
    	//Check if the Product exists
    	boolean existsProduct = productService.existsProduct(productId);
        if (!existsProduct) {
            return new ResponseEntity<Boolean>(false, HttpStatus.CONFLICT);
        }
        List<ProductRequest> lpr = productService.getAllProductRequest(productId);
        int taillePR = lpr.size();
        for (int indexPR = 0; indexPR < taillePR; indexPR++) {
            productRequestService.deleteProductRequest(lpr.get(indexPR).getId());
        }
        List<ProductBorrowing> lpb = productService.getAllProductBorrowing(productId);
        int taillePB = lpb.size();
        for (int indexPB = 0; indexPB < taillePB; indexPB++) {
            productBorrowingService.deleteProductBorrowing(lpb.get(indexPB).getId());
        }
        //Delete the Category from the DB
        productService.deleteProduct(productId);
        return new ResponseEntity<Boolean>(true, HttpStatus.NO_CONTENT);
    }

    /**
     * Get the list of all ProductDTO registered
     * @return
     */
    @GetMapping("/getAllProducts")
    @ApiOperation(value = "Get the List of all ProductDTO.", response = ProductDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "The List of ProductDTO is correctly retrived.") })
    public ResponseEntity<List<ProductDTO>> getAllProduct() {
        List<Product>  allProducts = productService.findAll();
        List<ProductDTO> allProductDTO = listProductToListProductDTO(allProducts);

        return new ResponseEntity<List<ProductDTO>>(allProductDTO, HttpStatus.OK);
    }

    /**
     * Convert a list of Product into a list of ProductDTO
     * @param productList : list to convert
     * @return
     */
    public List<ProductDTO> listProductToListProductDTO(List<Product> productList) {
        ArrayList<ProductDTO> productDTOs = new ArrayList<ProductDTO>();

        for (Product product : productList) {
        	ProductDTO temp = new ProductDTO(product.getId(), product.getBarcode(), product.getState(), 
            		product.isAvailable(),product.isReserved(),product.getArticle(), product.isTp(), product.isDonation(), product.isHs());
        	productDTOs.add(temp);
        }

        return productDTOs;
    }
    
    /**
     * Convert a Product into a ProductDTO
     * @param product : object to convert
     * @return
     */
    public ProductDTO productToProductDTO(Product product) {
        ProductDTO productDTO = new ProductDTO(product.getId(), product.getBarcode(), product.getState(), 
        		product.isAvailable(),product.isReserved(),product.getArticle(), product.isTp(), product.isDonation(), product.isHs());
        return productDTO;
    }
    

    /**
     * Convert a ProductDTO into a Product
     * @param productDTO : object to convert
     * @return
     */
    public Product productDTOtoProduct(ProductDTO productDTO) {
        return new Product(productDTO.getBarcode(), productDTO.getState(), 
        		productDTO.isAvailable(),productDTO.isReserved(),productDTO.getArticle(), productDTO.isTp(), productDTO.isDonation(), productDTO.isHs());
    }
}

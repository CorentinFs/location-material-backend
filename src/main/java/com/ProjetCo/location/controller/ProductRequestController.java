package com.ProjetCo.location.controller;


import com.ProjetCo.location.dto.ProductRequestDTO;
import com.ProjetCo.location.model.ProductBorrowing;
import com.ProjetCo.location.model.ProductRequest;
import com.ProjetCo.location.service.ProductBorrowingServiceImpl;
import com.ProjetCo.location.service.ProductRequestServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/rest/productrequest/api")
@Api(value = "ProductRequest Rest Controller: contains all operations for managing product request")
public class ProductRequestController {
    public static final Logger LOGGER = LoggerFactory.getLogger(ProductRequestDTO.class);

    @Autowired
    private ProductRequestServiceImpl productRequestService;
    @Autowired
    private ProductBorrowingServiceImpl productBorrowingService;
    
    /**
     * Get the List of all ProductRequests related to a User.
     * @param userId : the userId of the User.
     * @return
     */
    @GetMapping("/getProductRequestByUserId")
    @ApiOperation(value = "Get the List of all ProductRequest made by a User.", response = ProductRequestDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "The List of ProductRequest is correctly retrived.")})
    public ResponseEntity<List<ProductRequestDTO>> getProductRequestByUserId(@RequestParam(value = "userId") String userId){
    	List<ProductRequest>  allProductRequests = productRequestService.getProductRequestsByUserId(Long.parseLong(userId));
        List<ProductRequestDTO> allProductRequestDTO = listProductRequestToListProductRequestDTO(allProductRequests);

        return new ResponseEntity<List<ProductRequestDTO>>(allProductRequestDTO, HttpStatus.OK);
    }

    /**
     * Validate a productRequest
     * @param productRequestId : id of the productRequest to validate
     * @param adminComment
     * @return
     */
    @RequestMapping(value="/validateProductRequest/{productRequestId}", method = {RequestMethod.POST})
    @ApiOperation(value = "Delete a given ProductRequest", response = ProductRequestDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 204, message = "The ProductRequest is properly validated."),
                            @ApiResponse(code = 409, message = "The ProductRequest doesn't exist")})
    public ResponseEntity<Boolean> validateProductRequest(@PathVariable int productRequestId) {
        //Check if the ProductRequest exists
    	boolean existsProductRequest = productRequestService.existsIdProductRequest(productRequestId);
        if (!existsProductRequest) {
            return new ResponseEntity<Boolean>(false, HttpStatus.CONFLICT);
        }
        
        //If the ProductRequest exists then we can create a ProductBorrowing with the given parameters in the request
        ProductRequest pr = productRequestService.getProductRequest(productRequestId);
        ProductBorrowing pb = new ProductBorrowing(pr.getUserId(), pr.getStartDate(), pr.getEndDate(), null, pr.getProduct());
        productBorrowingService.saveProductBorrowing(pb);

        
        //Delete the request because we don't need it anymore
        productRequestService.deleteProductRequest(productRequestId);
        return new ResponseEntity<Boolean>(true, HttpStatus.NO_CONTENT);
    }
    
    /**
     * Insert a ProductRequest into the DB.
     * @return
     */
    @SuppressWarnings("unused")
	@PostMapping("/createProductRequest")
    @ApiOperation(value = "Add a new ProductRequest", response = ProductRequestDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 409, message = "The productRequest already exist"),
            				@ApiResponse(code = 201, message = "The productRequest is successfully inserted"),
            				@ApiResponse(code = 304, message = "The productRequest is unsuccessfully inserted")})
    public ResponseEntity<ProductRequestDTO> createProductRequest(@RequestBody ProductRequestDTO productRequestDTO) {
        //Check if the ProductRequest exists
    	boolean existsProductRequest = productRequestService.existsIdProductRequest(productRequestDTO.getId());
        if (existsProductRequest) {
            return new ResponseEntity<ProductRequestDTO>(HttpStatus.CONFLICT);
        }

        //Save the new ProductRequest into the DB
        ProductRequest prRequest = productRequestDTOtoProductRequest(productRequestDTO);
        ProductRequest productRequest = productRequestService.saveProductRequest(prRequest);
        productRequestDTO.setId(productRequest.getId());

        if (productRequest != null) {
            return new ResponseEntity<ProductRequestDTO>(productRequestDTO, HttpStatus.CREATED);
        }

        return new ResponseEntity<ProductRequestDTO>(HttpStatus.NOT_MODIFIED);
    }

    /**
     * Delete a ProductRequest from the DB.
     * @param productRequestId : id of the ProductRequest to delete
     * @return
     */
    @RequestMapping(value="/deleteProductRequest/{productRequestId}", method = {RequestMethod.DELETE, RequestMethod.POST})
    @ApiOperation(value = "Delete a given ProductRequest", response = ProductRequestDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 204, message = "The ProductRequest is properly deleted."),
    						@ApiResponse(code = 409, message = "The ProductRequest doesn't exist")})
    public ResponseEntity<Boolean> deleteProductRequest(@PathVariable int productRequestId) {
    	//Check if the ProductRequest exists
    	boolean existsProductRequest = productRequestService.existsIdProductRequest(productRequestId);
        if (!existsProductRequest) {
            return new ResponseEntity<Boolean>(false, HttpStatus.CONFLICT);
        }

        //Delete the Category from the DB
        productRequestService.deleteProductRequest(productRequestId);
        return new ResponseEntity<Boolean>(true, HttpStatus.NO_CONTENT);
    }

    /**
     * Get the list of all ProductRequestDTO registered
     * @return
     */
    @GetMapping("/getAllProductRequests")
    @ApiOperation(value = "Get the List of all ProductRequestDTO.", response = ProductRequestDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "The List of ProductRequestDTO is correctly retrived.") })
    public ResponseEntity<List<ProductRequestDTO>> getAllProductRequest() {
        List<ProductRequest>  allProductRequests = productRequestService.findAll();
        List<ProductRequestDTO> allProductRequestDTO = listProductRequestToListProductRequestDTO(allProductRequests);

        return new ResponseEntity<List<ProductRequestDTO>>(allProductRequestDTO, HttpStatus.OK);
    }

    /**
     * Convert a list of ProductRequest into a list of ProductRequestDTO
     * @param productRequestList
     * @return
     */
    public List<ProductRequestDTO> listProductRequestToListProductRequestDTO(List<ProductRequest> productRequestList) {
        ArrayList<ProductRequestDTO> productRequestDTOs = new ArrayList<ProductRequestDTO>();

        for (ProductRequest productRequest : productRequestList) {
            ProductRequestDTO temp = new ProductRequestDTO(productRequest.getId(), productRequest.getUserId(), productRequest.getStartDate(), productRequest.getEndDate(),productRequest.getProduct());
            productRequestDTOs.add(temp);
        }

        return productRequestDTOs;
    }

    /**
     * Convert a ProductRequestDTO into a ProductRequest
     * @param productRequestDTO
     * @return
     */
    public ProductRequest productRequestDTOtoProductRequest(ProductRequestDTO productRequestDTO) {
        return new ProductRequest( productRequestDTO.getUserId(), productRequestDTO.getStartDate(), productRequestDTO.getEndDate(), productRequestDTO.getProduct());
    }
}

package com.ProjetCo.location.controller;


import com.ProjetCo.location.dto.ProductBorrowingDTO;
import com.ProjetCo.location.model.ProductBorrowing;
import com.ProjetCo.location.service.ProductBorrowingServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/rest/productborrowing/api")
@Api(value = "ProductBorrowing Rest Controller: contains all operations for managing product borrowing")
public class ProductBorrowingController {
    public static final Logger LOGGER = LoggerFactory.getLogger(ProductBorrowingDTO.class);

    @Autowired
    private ProductBorrowingServiceImpl productBorrowingService;

    /**
     * Update a ProductBorrowing
     * @param ProductBorrowingDTO corresponding to the ProductBorrowing to update
     * @return
     */
    @PutMapping("/updateProductBorrowing")
    @ApiOperation(value = "Update a ProductBorrowing.", response = ProductBorrowingDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 409, message = "The ProductBorrowing doesn't exist."),
    						@ApiResponse(code = 201, message = "The ProductBorrowing is correctly updated."),
    						@ApiResponse(code = 304, message = "The ProductBorrowing is unsuccessfully updated")})
    public ResponseEntity<ProductBorrowingDTO> updateProductBorrowing(@RequestBody ProductBorrowingDTO productBorrowingDTO) {
        //Check if the productBorrowing exists
    	if (!productBorrowingService.existsProductBorrowing(productBorrowingDTO.getId())) {
            return new ResponseEntity<ProductBorrowingDTO>(HttpStatus.CONFLICT);
        }
        ProductBorrowing productBorrowingRequest = productBorrowingService.getProductBorrowing(productBorrowingDTO.getId());
        
        //Update
        productBorrowingRequest.setAdminComment(productBorrowingDTO.getAdminComment());
        productBorrowingRequest.setEndDate(productBorrowingDTO.getEndDate());
        productBorrowingRequest.setPickedUp(productBorrowingDTO.getPickedUp());
        productBorrowingRequest.setRenderDate(productBorrowingDTO.getRenderDate());
        productBorrowingRequest.setProduct(productBorrowingDTO.getProduct());
        productBorrowingRequest.setStartDate(productBorrowingDTO.getStartDate());
        productBorrowingRequest.setUserId(productBorrowingDTO.getUserId());
        
        ProductBorrowing productResponse = productBorrowingService.saveProductBorrowing(productBorrowingRequest);
        
        if (productResponse != null) {
            return new ResponseEntity<ProductBorrowingDTO>(productBorrowingDTO, HttpStatus.CREATED);
        }
        return new ResponseEntity<ProductBorrowingDTO>(HttpStatus.NOT_MODIFIED);
    }
    
    /**
     * Get the List of all returned ProductBorrowing related to a User.
     * @param userId : the userId of the User.
     * @return
     */
    @GetMapping("/getReturnedProductBorrowingByUserId")
    @ApiOperation(value = "Get the List of all returned ProductBorrowing made by a User.", response = ProductBorrowingDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "The List of returned ProductBorrowing made by a user is correctly retrieved.")})
    public ResponseEntity<List<ProductBorrowingDTO>> getReturnedProductBorrowingByUserId(@RequestParam(value = "userId") String userId){
        List<ProductBorrowing>  allProductBorrowing = productBorrowingService.getReturnedProductBorrowingByUserId(Long.parseLong(userId));
        List<ProductBorrowingDTO> allProductBorrowingDTO = listProductBorrowingToListProductBorrowingDTO(allProductBorrowing);

        return new ResponseEntity<List<ProductBorrowingDTO>>(allProductBorrowingDTO,HttpStatus.OK);
    }

    /**
     * Get the List of all ProductBorrowing related to a User.
     * @param userId : the userId of the User.
     * @return
     */
    @GetMapping("/getProductBorrowingByUserId")
    @ApiOperation(value = "Get the List of all ProductBorrowing made by a User.", response = ProductBorrowingDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "The List of ProductBorrowing made by a user is correctly retrieved.")})
    public ResponseEntity<List<ProductBorrowingDTO>> getProductBorrowingByUserId(@RequestParam(value = "userId") String userId){
        List<ProductBorrowing>  allProductBorrowing = productBorrowingService.getProductBorrowingByUserId(Long.parseLong(userId));
        List<ProductBorrowingDTO> allProductBorrowingDTO = listProductBorrowingToListProductBorrowingDTO(allProductBorrowing);

        return new ResponseEntity<List<ProductBorrowingDTO>>(allProductBorrowingDTO,HttpStatus.OK);
    }

    @GetMapping("/getProductBorrowing")
    @ApiOperation(value = "Get a ProductBorrowing with other parameters", response = ProductBorrowingDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "The ProductBorrowing is correctly retrieved.")})
    public ResponseEntity<ProductBorrowingDTO> getProductBorrowing(@RequestParam(value = "userId") String userId,
                                                                   @RequestParam(value = "startDate") String startDate,
                                                                   @RequestParam(value = "endDate") String endDate,
                                                                   @RequestParam(value = "productId") String productId) throws ParseException {
    	System.out.println("########");
    	System.out.println(startDate);
    	System.out.println(endDate);
    	
    	LocalDate start_Date = LocalDate.parse(startDate);
    	System.out.println(start_Date);
    	LocalDate end_Date = LocalDate.parse(endDate);
    	System.out.println(end_Date);
    	
        ProductBorrowing productBorrowing = productBorrowingService.getProductBorrowingWithParam(Long.parseLong(userId), start_Date, end_Date, Integer.parseInt(productId));
        
        System.out.println(productBorrowing);
        
        ProductBorrowingDTO productBorrowingDTO = productBorrowingToProductBorrowingDTO(productBorrowing);
        if (productBorrowingDTO != null) {
            return new ResponseEntity<ProductBorrowingDTO>(productBorrowingDTO, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<ProductBorrowingDTO>(HttpStatus.NO_CONTENT);
        }
    }

    /**
     * Insert a ProductBorrowing into the DB.
     * @return
     */
    @SuppressWarnings("unused")
	@PostMapping("/createProductBorrowing")
    @ApiOperation(value = "Add a new ProductBorrowing to the database", response = ProductBorrowingDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 409, message = "The ProductBorrowing already exist"),
            				@ApiResponse(code = 201, message = "The ProductBorrowing is successfully inserted"),
            				@ApiResponse(code = 304, message = "The ProductBorrowing is unsuccessfully inserted")})
    public ResponseEntity<ProductBorrowingDTO> createProductBorrowing(@RequestBody ProductBorrowingDTO productBorrowingDTO) {
        //Check if the productBorrowing already exists
    	boolean existsProductBorrowing = productBorrowingService.existsIdProductBorrowing(productBorrowingDTO.getId());
        if (existsProductBorrowing) {
            return new ResponseEntity<ProductBorrowingDTO>(HttpStatus.CONFLICT);
        }

        //Save the new ProductBorrowing into the DB
        ProductBorrowing pbRequest = productBorrowingDTOtoProductBorrowing(productBorrowingDTO);
        ProductBorrowing productBorrowing = productBorrowingService.saveProductBorrowing(pbRequest);
        productBorrowingDTO.setId(productBorrowing.getId());

        if (productBorrowing != null) {
            return new ResponseEntity<ProductBorrowingDTO>(productBorrowingDTO, HttpStatus.CREATED);
        }

        return new ResponseEntity<ProductBorrowingDTO>(HttpStatus.NOT_MODIFIED);
    }

    /**
     * Delete a ProductBorrowing from the DB.
     * @param productBorrowingId : id of the ProductBorrowing to delete
     * @return
     */
    @RequestMapping(value="/deleteProductBorrowing/{productBorrowingId}", method = {RequestMethod.DELETE, RequestMethod.POST})
    @ApiOperation(value = "Delete a given ProductBorrowing", response = ProductBorrowingDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 204, message = "The ProductBorrowing is properly deleted."),
    						@ApiResponse(code = 409, message = "The ProductBorrowing doesn't exist.")})
    public ResponseEntity<Boolean> deleteProductBorrowing(@PathVariable int productBorrowingId) {
    	//Check if the ProductBorrowing exists
    	boolean existsProductBorrowing = productBorrowingService.existsIdProductBorrowing(productBorrowingId);
        if (!existsProductBorrowing) {
            return new ResponseEntity<Boolean>(false, HttpStatus.CONFLICT);
        }

        //Delete the ProductBorrowing from the DB
        productBorrowingService.deleteProductBorrowing(productBorrowingId);
        return new ResponseEntity<Boolean>(true, HttpStatus.NO_CONTENT);
    }

    /**
     * Get the list of all ProductBorrowingDTO registered
     * @return
     */
    @GetMapping("/getAllProductBorrowings")
    @ApiOperation(value = "Get the List of all ProductBorrowingDTO.", response = ProductBorrowingDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "The List of ProductBorrowingDTO is correctly retrieved.") })
    public ResponseEntity<List<ProductBorrowingDTO>> getAllProductBorrowing() {
        List<ProductBorrowing>  allProductBorrowings = productBorrowingService.getAllProductBorrowing();
        List<ProductBorrowingDTO> allProductBorrowingDTO = listProductBorrowingToListProductBorrowingDTO(allProductBorrowings);

        return new ResponseEntity<List<ProductBorrowingDTO>>(allProductBorrowingDTO, HttpStatus.OK);
    }

    /**
     * Convert a list of ProductBorrowing into a list of ProductBorrowingDTO
     * @param productBorrowingList
     * @return
     */
    public List<ProductBorrowingDTO> listProductBorrowingToListProductBorrowingDTO(List<ProductBorrowing> productBorrowingList) {
        ArrayList<ProductBorrowingDTO> productBorrowingDTOs = new ArrayList<ProductBorrowingDTO>();

        for (ProductBorrowing productBorrowing : productBorrowingList) {
            ProductBorrowingDTO temp = new ProductBorrowingDTO(productBorrowing.getId(), productBorrowing.getUserId(), productBorrowing.getStartDate(), productBorrowing.getEndDate(), productBorrowing.getRenderDate(), productBorrowing.getAdminComment(), productBorrowing.getPickedUp(), productBorrowing.getProduct());
            productBorrowingDTOs.add(temp);
        }

        return productBorrowingDTOs;
    }

    /**
     * Convert a ProductBorrowingDTO into a ProductBorrowing
     * @param productBorrowingDTO : the object to convert
     * @return
     */
    public ProductBorrowing productBorrowingDTOtoProductBorrowing(ProductBorrowingDTO productBorrowingDTO) {
        return new ProductBorrowing(productBorrowingDTO.getUserId(), productBorrowingDTO.getStartDate(), productBorrowingDTO.getEndDate(), productBorrowingDTO.getRenderDate(), productBorrowingDTO.getAdminComment(), productBorrowingDTO.getPickedUp(), productBorrowingDTO.getProduct());
    }
    
    /**
     * Convert a ProductBorrowing into a ProductBorrowingDTO
     * @param productBorrowing : the object to convert
     * @return
     */
    public ProductBorrowingDTO productBorrowingToProductBorrowingDTO(ProductBorrowing productBorrowing) {
        return new ProductBorrowingDTO(productBorrowing.getId(), productBorrowing.getUserId(), productBorrowing.getStartDate(), productBorrowing.getEndDate(), productBorrowing.getRenderDate(), productBorrowing.getAdminComment(), productBorrowing.getPickedUp(), productBorrowing.getProduct());
    }
}

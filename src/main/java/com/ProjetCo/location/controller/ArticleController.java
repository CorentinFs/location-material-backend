package com.ProjetCo.location.controller;

import com.ProjetCo.location.dto.ArticleDTO;
import com.ProjetCo.location.dto.ProductDTO;
import com.ProjetCo.location.dto.ProductIndisponibilityDTO;
import com.ProjetCo.location.model.Article;
import com.ProjetCo.location.model.Product;
import com.ProjetCo.location.model.ProductBorrowing;
import com.ProjetCo.location.service.ArticleServiceImpl;
import com.ProjetCo.location.service.ProductBorrowingServiceImpl;
import com.ProjetCo.location.service.ProductServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/rest/article/api")
@Api(value = "Article Rest Controller: contains all operations for managing article")
public class ArticleController {
    public static final Logger LOGGER = LoggerFactory.getLogger(ArticleController.class);

    @Autowired
    private ArticleServiceImpl articleService;
    
    @Autowired
    private ProductServiceImpl productService;
    
    @Autowired
    private ProductBorrowingServiceImpl productBorrowingService;

    /**
     * Update an Article from the database
     * @param articleDTO corresponding to the Article to update
     * @return
     */
    @PutMapping("/updateArticle")
    @ApiOperation(value = "Update a Article from the database.", response = ArticleDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 409, message = "The article doesn't exist"),
            				@ApiResponse(code = 201, message = "The article is successfully updated"),
            				@ApiResponse(code = 304, message = "The article is unsuccessfully updated")})
    public ResponseEntity<ArticleDTO> updateArticle(@RequestBody ArticleDTO articleDTO) {
    	//Check if the Article exists
    	if (!articleService.existsArticle(articleDTO.getId())) {
            return new ResponseEntity<ArticleDTO>(HttpStatus.CONFLICT);
        }
    	
        Article articleRequest = articleService.getArticle(articleDTO.getName());
        
        //Update
        articleRequest.setCategory(articleDTO.getCategory());
        articleRequest.setDescription(articleDTO.getDescription());
        articleRequest.setImage(articleDTO.getImage());
        articleRequest.setName(articleDTO.getName());
        articleRequest.getProducts().clear();
        articleRequest.getProducts().addAll(articleDTO.getProducts());
        
        Article articleResponse = articleService.saveArticle(articleRequest);
        
        if (articleResponse != null) {
        	ArticleDTO articleDTOToReturn = articleToArticleDTO(articleResponse);
            return new ResponseEntity<ArticleDTO>(articleDTOToReturn, HttpStatus.CREATED);
        }
        return new ResponseEntity<ArticleDTO>(HttpStatus.NOT_MODIFIED);
    }
    
    /**
     * Get the dates when the products related to an Article are not available.
     * Returns a ProductIndisponibilityDTO which contains the product and the list of indisponibilities.
     * It will contains the start date and end date of the borrowing if the endDate is not before today.
     * It will contains the start date and an empty string if the endDate if past but the borrowing hasn't been returned. (so render date is null)
     * @param id : the id of the article to get not available dates of products linked to it.
     * @return List<List<String>> : a list of couple of dates representing the start and the end of the unavailability.
     */
    @GetMapping("/getNotAvailableProducts")
    @ApiOperation(value = "Get the dates when the products related to an Article are not available."
    		+ "Returns a ProductIndisponibilityDTO which contains the product and the list of indisponibilities."
    		+ "It will contains the start date and end date of the borrowing if the endDate is not before today."
    		+ "It will contains the start date and an empty string if the endDate if past but the borrowing hasn't been returned. (so render date is null)", 
    		response = ProductIndisponibilityDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 409, message = "Conflict : the id does not correspond to an Article, "
    															+ "or the Article doesn't have a related Product."),
    						@ApiResponse(code = 200, message = "The List of dates is correctly returned.") })
    public ResponseEntity<List<ProductIndisponibilityDTO>> getNotAvailableProducts(@RequestParam(name = "id") int id)
    {
    	//Check if the Article exists.
    	Optional<Article> article = articleService.getArticle(id);
    	
    	if(!article.isPresent()) {
    		return new ResponseEntity<List<ProductIndisponibilityDTO>>(HttpStatus.CONFLICT);
    	}
    	
    	//Get the Products related to the Article, and check if there is at least one Product
    	List<Product> allProducts = productService.getProductsOfArticle(article.get());
    	
    	if(allProducts.size() == 0) {
    		return new ResponseEntity<List<ProductIndisponibilityDTO>>(HttpStatus.CONFLICT);
    	}
    	
    	List<ProductIndisponibilityDTO> allIndisponibilityDTOs = new ArrayList<ProductIndisponibilityDTO>();
    	
    	for(Product product : allProducts)
    	{
    	    List<List<String>> allDates = new ArrayList<List<String>>();
    	    
    		List<ProductBorrowing> allProductBorrowings = productBorrowingService.getNotAvailableProductBorrowing(product, LocalDate.now());
    		
    		//Construct a ProductIndisponibility object
			ProductIndisponibilityDTO productIndisponibilityDTO = new ProductIndisponibilityDTO();
			productIndisponibilityDTO.setProductDTO(productToProductDTO(product));
    		
    		for(ProductBorrowing pb : allProductBorrowings)
    		{    			
    			List<String> dates = new ArrayList<String>();
    			
    			//Formatting the date as dd-MM-yyyy
    			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    			String startDate = pb.getStartDate().format(formatter);    			  
    			
    			dates.add(startDate);
    			
    			if(pb.getRenderDate() == null && pb.getEndDate().isAfter(LocalDate.now())) {
    				String endDate = pb.getEndDate().format(formatter);
					dates.add(endDate);
    			}
    			else {
    				dates.add("");
    			}
    			
    			allDates.add(dates);
    		}
    		
    		//Add all the dates into the DTO
    		productIndisponibilityDTO.setIndisponibilityDates(allDates);
    		
    		allIndisponibilityDTOs.add(productIndisponibilityDTO);
    	}
    	
    	return new ResponseEntity<List<ProductIndisponibilityDTO>>(allIndisponibilityDTOs, HttpStatus.OK);
    }
    
    /**
     * Get an Article given a name.
     * @param name : the name of the Article to search in the database.
     * @return
     */
    @GetMapping("/getArticleByName")
    @ApiOperation(value = "Get the Article corresponding to the given name.", response = ArticleDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 409, message = "Conflict : the name does not correspond to an Article."),
    						@ApiResponse(code = 200, message = "The Article is correctly returned.") })
    public ResponseEntity<ArticleDTO> getArticleByName(@RequestParam(name = "name") String name)
    {
    	boolean existsName = articleService.existsArticle(name);
    	if(!existsName) {
    		return new ResponseEntity<ArticleDTO>(HttpStatus.CONFLICT);
    	}
    	
    	Article article = articleService.getArticle(name);
    	ArticleDTO articleDTO = articleToArticleDTO(article);
    	
    	return new ResponseEntity<ArticleDTO>(articleDTO, HttpStatus.OK);
    }
    
    /**
     * Get an Article given an id.
     * @param id : the id of the Article to search in the database.
     * @return
     */
    @GetMapping("/getArticleById")
    @ApiOperation(value = "Get the Article corresponding to the given id.", response = ArticleDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 409, message = "Conflict : the id does not correspond to an Article."),
    						@ApiResponse(code = 200, message = "The Article is correctly returned.") })
    public ResponseEntity<ArticleDTO> getArticleById(@RequestParam(value = "id") int id){
    	Optional<Article> article = articleService.getArticle(id);
    	
    	if(article.isPresent()) {
    		ArticleDTO articleDTO = articleToArticleDTO(article.get());
    		return new ResponseEntity<ArticleDTO>(articleDTO,HttpStatus.CREATED);
    	}
    	
    	return new ResponseEntity<ArticleDTO>(HttpStatus.CONFLICT);
    }
    
    /**
     * Insert an Article into the DB.
     * If the Article already exists, return HTTP_CONFLICT
     * If the Article doesn't exist and is properly added, return HTTP_CREATED
     * @param name : name of the Article to add
     * @param description : description of the Article to add
     * @param category : category of the Article to add
     * @return
     */
    @SuppressWarnings("unused")
	@PostMapping("/createArticle")
    @ApiOperation(value = "Add a new Article", response = ArticleDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 409, message = "Conflict: the article already exist"),
            @ApiResponse(code = 201, message = "Created: the article has been successfully inserted"),
            @ApiResponse(code = 304, message = "Not Modified: the article has been unsuccessfully inserted") })
    public ResponseEntity<ArticleDTO> createArticle(@RequestBody ArticleDTO articleDTO) {
        boolean existsArticle = articleService.existsArticle(articleDTO.getName());
        if (existsArticle) {
            return new ResponseEntity<ArticleDTO>(HttpStatus.CONFLICT);
        }

        //Save the new Article into the DB
        Article articleRequest = articleDTOtoArticle(articleDTO);
        Article article = articleService.saveArticle(articleRequest);
        articleDTO.setId(article.getId());
        if(article != null) {
            return new ResponseEntity<ArticleDTO>(articleDTO,HttpStatus.CREATED);
        }

        return new ResponseEntity<ArticleDTO>(HttpStatus.NOT_MODIFIED);
    }
    
    /**
     * Delete an Article from the DB.
     * If the Article doesn't exists, return HTTP_CONFLICT
     * If the Article exists and is properly deleted, return HTTP_NO_CONTENT
     * @param name : name of the Article to delete
     * @return
     */
    @RequestMapping(value="/deleteArticle/{articleId}", method = {RequestMethod.DELETE, RequestMethod.POST})
    @ApiOperation(value = "Delete a given Article", response = ArticleDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 204, message = "The Article is properly deleted.") })
    public ResponseEntity<Boolean> deleteArticle(@PathVariable Integer articleId) {
        //check if the article exists
        boolean existsArticle = articleService.existsArticle(articleId);
        if (!existsArticle) {
            return new ResponseEntity<Boolean>(false, HttpStatus.CONFLICT);
        }

        //delete the Article from the DB
        articleService.deleteArticle(articleId);
        return new ResponseEntity<Boolean>(true, HttpStatus.NO_CONTENT);
    }

    /**
     * Get the list of all ArticleDTO registered
     * @return
     */
    @GetMapping("/getAllArticles")
    @ApiOperation(value = "Get the List of all ArticleDTO.", response = ArticleDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "The List of ArticleDTO is correctly retrived.") })
    public ResponseEntity<List<ArticleDTO>> getAllArticle() {
        List<Article>  allArticles = articleService.findAll();
        List<ArticleDTO> allArticleDTO = listArticleToListArticleDTO(allArticles);

        return new ResponseEntity<List<ArticleDTO>>(allArticleDTO, HttpStatus.OK);
    }

    /**
     * Convert a list of Article into a list of ArticleDTO
     * @param ArticleList
     * @return
     */
    public List<ArticleDTO> listArticleToListArticleDTO(List<Article> articleList) {
        ArrayList<ArticleDTO> articleDTOs = new ArrayList<ArticleDTO>();

        for (Article article : articleList) {
            ArticleDTO temp = new ArticleDTO(article.getId(), 
            		article.getName(), 
            		article.getDescription(),
            		article.getCategory(),
            		article.getProducts(),
            		article.getImage());
            articleDTOs.add(temp);
        }

        return articleDTOs;
    }
    
    /**
     * Convert a Product into a ProductDTO
     * @param product : object to convert
     * @return
     */
    public ProductDTO productToProductDTO(Product product) {
        ProductDTO productDTO = new ProductDTO(product.getId(), product.getBarcode(), product.getState(), 
        		product.isAvailable(),product.isReserved(),product.getArticle(), product.isTp(), product.isDonation(), product.isHs());
        return productDTO;
    }
    
    /**
     * Transform a Article into a ArticleDTO entity.
     * @param article : the Article to transform into a DTO.
     * @return ArticleDTO : the ArticleDTO entity corresponding.
     */
    public ArticleDTO articleToArticleDTO(Article article) {
    	return new ArticleDTO(article.getId(), article.getName(), article.getDescription(),
    			article.getCategory(),article.getProducts(), article.getImage());
    }
    
    /**
     * Transform a ArticleDTO into a Article entity object.
     * @param categoryDTO : the articleDTO To transform.
     * @return Category : the Article entity corresponding.
     */
    public Article articleDTOtoArticle(ArticleDTO articleDTO) {
        return new Article(articleDTO.getName(), articleDTO.getDescription(),
        		articleDTO.getCategory(),articleDTO.getProducts(), articleDTO.getImage());
    }
}

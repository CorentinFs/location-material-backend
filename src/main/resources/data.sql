-- All Deletes

DELETE FROM User WHERE userId = 356012453;
DELETE FROM User WHERE userId = 450125673;
DELETE FROM User WHERE userId = 213451230;
DELETE FROM User WHERE userId = 123456789;
DELETE FROM User WHERE userId = 987654321;
DELETE FROM User WHERE userId = 123412341;

DELETE FROM ConsumableBorrowing WHERE id = 1;
DELETE FROM ConsumableBorrowing WHERE id = 2;

DELETE FROM ConsumableRequest WHERE id = 1;
DELETE FROM ConsumableRequest WHERE id = 2;

DELETE FROM ProductRequest WHERE id = 1;
DELETE FROM ProductRequest WHERE id = 2;

DELETE FROM ProductBorrowing WHERE id = 1;
DELETE FROM ProductBorrowing WHERE id = 2;
DELETE FROM ProductBorrowing WHERE id = 3;

DELETE FROM Product WHERE id = 1;
DELETE FROM Product WHERE id = 2;

DELETE FROM Article WHERE name = "Article 1";
DELETE FROM Article WHERE name = "Article 2";

DELETE FROM Category WHERE name = "Category 1";
DELETE FROM Category WHERE name = "Category 2";

-- Users

INSERT INTO User (id, userId, password, isAdmin, isSuperAdmin) VALUES(1, 356012453, "0b14d501a594442a01c6859541bcb3e8164d183d32937b851835442f69d5c94e", false, false); -- password1
INSERT INTO User (id, userId, password, isAdmin, isSuperAdmin) VALUES(2, 450125673, "6cf615d5bcaac778352a8f1f3360d23f02f34ec182e259897fd6ce485d7870d4", false, false); -- password2
INSERT INTO User (id, userId, password, isAdmin, isSuperAdmin) VALUES(3, 213451230, "5906ac361a137e2d286465cd6588ebb5ac3f5ae955001100bc41577c3d751764", false, false); -- password3
INSERT INTO User (id, userId, password, isAdmin, isSuperAdmin) VALUES(4, 123456789, "b97873a40f73abedd8d685a7cd5e5f85e4a9cfb83eac26886640a0813850122b", true, false); -- password4
INSERT INTO User (id, userId, password, isAdmin, isSuperAdmin) VALUES(5, 987654321, "8b2c86ea9cf2ea4eb517fd1e06b74f399e7fec0fef92e3b482a6cf2e2b092023", true, false); -- password5
INSERT INTO User (id, userId, password, isAdmin, isSuperAdmin) VALUES(6, 123412341, "598a1a400c1dfdf36974e69d7e1bc98593f2e15015eed8e9b7e47a83b31693d5", false, true); -- password6

-- Consummable Borrowing

INSERT INTO ConsumableBorrowing (id, name, userId, startDate, endDate, renderDate)
			Values(1, 'Consommable 1', 123456789, '2020-04-21', '2020-04-27', '2020-04-27');
INSERT INTO ConsumableBorrowing (id, name, userId, startDate, endDate, renderDate)
			Values(2, 'Consommable 2', 123456789, '2020-04-22', '2020-04-23', '2020-04-23');
			
-- Consummable Requests

INSERT INTO ConsumableRequest (id, name, userId, startDate, endDate)
			Values(1, 'Consommable 1', 123456789, '2020-05-01', '2020-05-12');
INSERT INTO ConsumableRequest (id, name, userId, startDate, endDate)
			Values(2, 'Consommable 2', 123456789, '2020-05-21', '2020-05-23');
			
-- Category

INSERT INTO Category (id, name) VALUES(1, "Category 1");
INSERT INTO Category (id, name) VALUES(2, "Category 2");

-- Article

INSERT INTO Article (id, name, description, category, image) VALUES(1, "Article 1", "Description 1", 1, NULL);
INSERT INTO Article (id, name, description, category, image) VALUES(2, "Article 2", "Description 2", 2, NULL);

-- Product

INSERT INTO Product (id, barcode, state, isavailable, isreserved, article, tp, donation, hs) 
			VALUES(1, 6512216498465, "Neuf", TRUE, FALSE, 1, FALSE, FALSE, FALSE);
INSERT INTO Product (id, barcode, state, isavailable, isreserved, article, tp, donation, hs) 
			VALUES(2, 5465465498798, "Neuf", TRUE, TRUE, 2, FALSE, FALSE, FALSE);
			
-- Product Borrowing

INSERT INTO ProductBorrowing (id, userId, startDate, endDate, renderDate, adminComment, pickedUp, product)
			Values(1, 123456789, '2020-04-21', '2020-05-18', NULL, "", FALSE, 1);
INSERT INTO ProductBorrowing (id, userId, startDate, endDate, renderDate, adminComment, pickedUp, product)
			Values(2, 123456789, '2020-04-22', '2020-05-23', '2020-05-19', "", TRUE, 2);
INSERT INTO ProductBorrowing (id, userId, startDate, endDate, renderDate, adminComment, pickedUp, product)
			Values(3, 123456789, '2020-04-21', '2020-12-27', NULL, "", FALSE, 1);

-- Product Request

INSERT INTO ProductRequest (id, userId, startDate, endDate, product)
			Values(1, 123456789, '2020-04-21', '2020-04-27', 1);
INSERT INTO ProductRequest (id, userId, startDate, endDate, product)
			Values(2, 123456789, '2020-04-22', '2020-04-23', 2);
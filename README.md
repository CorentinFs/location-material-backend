**Resssources nécessaires :** 
*   Wamp avec une Bd MySql 
*   Créer une Bd "location_materiel_db"
    
**Paramètrage du projet :** 
Dans src/main/ressources : 
*  entrer user name et mdp de la Bd
*  dans db.url, changer le port de la Bd
                            
**Déploiement de la Bd et lancement du serveur :**
*  lancer src/main/java/com/ProjetCo/location/LocationApplication.java

**Logs des users :**
**SuperAdmin :**
* userId : 123412341 / password : password6

**Admins :**
* userId : 987654321 / password : password5
* userId : 123456789 / password : password4

**Users :**
* userId : 356012453 / password : password1
* userId : 450125673 / password : password2
* userId : 213451230 / password : password3